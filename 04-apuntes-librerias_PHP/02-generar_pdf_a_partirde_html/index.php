<?php

//cargamos autoload de la libreria html2pdf
require_once dirname(__FILE__).'/../vendor/autoload.php'; // es similar a require_once '../vendor/autoload.php';

//cargar libreria
use Spipu\Html2Pdf\Html2Pdf ;

//crar objeto Html2pdf

$html2pdf = new Html2Pdf();

//recoger la vista a imprimir
//abrimos con el ob_start y todo lo que se abra despues en el buffer lo recoge y 
//lo guardamos con el objeto ob_get_clean que recoge lo almacenado en el buffer
ob_start();
require_once 'pdf_a_generar.php';
$html = ob_get_clean();

$html2pdf->writeHTML($html);

$name = "pdf_test.pdf";
$html2pdf->output($name);