<?php

/* Cargamos la libreria con el autoload.php para poder utilizar el objeto */
require_once dirname(__FILE__) . '/../vendor/autoload.php';

$foto_original = "img/porche.jpeg";
$name_for_save = 'porche_mod.jpeg';

/* creamos un nuevo objeto con la foto a modificar */
$thumb = new PHPThumb\GD($foto_original);

/* vamos por ejemplo a redimensionar la foto  */
//$maxWidth = 250; //125px
//$maxHeight = 250;

$thumb->resize(450, 450);

/* recortar imagen */
//$maxHeight_cr = 50;
//$maxWidth_cr = 50;
//$cropWidth = 120;
//$cropHeight = 120;

$thumb->crop(50, 50, 120, 120);
		

/*Mostrar foto */
$thumb->show();

/* Guardar foto modificada*/
$thumb->save($name_for_save);

?>

