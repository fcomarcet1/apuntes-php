<?php

//cargamos autoload de la libreria html2pdf
require_once dirname(__FILE__).'/../vendor/autoload.php'; // es similar a require_once '../vendor/autoload.php';

//cargar libreria
use Spipu\Html2Pdf\Html2Pdf ;

//crar objeto Html2pdf

$html2pdf = new Html2Pdf();

$html = "<h1>Creando un pdf con la libreria de php html2pdf</h1>"
		. "<h2>Creando el primer pdf</h2>"
		. "<p>Test del pdf</p>";

$html2pdf->writeHTML($html);

$name = "test_html2pdf.pdf";
$html2pdf->output($name);