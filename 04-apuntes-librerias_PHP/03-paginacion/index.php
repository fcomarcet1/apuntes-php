<?php

include_once dirname(__FILE__).'/../vendor/autoload.php';

//variables BD
$hostdb = "localhost";
$userdb = "admin";
$password_db = "admin";
$database = "notas_master";
$portdb = 3306;

//conexion
$conexion = new mysqli($hostdb, $userdb, $password_db, $database, $portdb);
$conexion->query("SET NAMES 'utf8'");

//consulta para obtener nº de elementos por pagina
$sql = "SELECT * FROM notas";
$result = $conexion->query($sql);
$num_rows = $result->num_rows;
$num_elem_pagina = 2;

/*
 * similar pero mas optimo
/ $consulta = $conexion->query("SELECT COUNT(id) as 'total' FROM notas");
/ $num_rows = $consulta->fetch_object()->total;
/ $num_elem_pagina = 2;
*/

//Paginacion
$pagination = new Zebra_Pagination();

//nº total de elemantos a paginar
$pagination->records($num_rows);

//nº de elemntos por pagina
$pagination->records_per_page($num_elem_pagina);

//con get_page obtenemos el nº de pagina de la url
$pagina_actual = $pagination->get_page();
$inicio = (($pagina_actual-1)*$num_elem_pagina);

//realizamos una consulta con la clausula LIMIT indicando el nº de elemntos que deseamos por pagina
$sql_limit = "SELECT * FROM notas LIMIT $inicio,$num_elem_pagina";
$notas = $conexion->query($sql_limit);

//var_dump($sql_limit);die();

//estilos
echo '<link rel="stylesheet" href="../vendor/stefangabos/zebra_pagination/public/css/zebra_pagination.css" type="text/css">';

while ($nota = $notas->fetch_object()) {
	echo "<h1>{$nota->titulo}</h1>";
	echo "<h3>{$nota->descripcion}</h3>"."<hr/>";
	
}

//links de la paginacion
$pagination->render();

