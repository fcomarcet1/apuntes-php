<?php

class UsuarioController {
	
	//controlador ejecuta las posibles acciones o metodos
	
	public function mostrarTodos() {
		
		//cargar modelo
		require_once 'models/usuario.php';
		
		//crear nuevo objeto
		$usuario = new Usuario();
		
		$tabla = 'usuarios';
		$conseguir_todos = $usuario->ConseguirTodos($tabla);
		
		//cargamos la vista aqui y no al principio y asi tendremos cargado el modelo y el objeto con sus metodos
		require_once 'views/usuario/mostrarTodos.php';	
	}
	
	public function crearUsuario() {
		require_once 'views/usuario/crearusuario.php';
	}
	
	public function borrarUsuario() {
		
	}
	
	public function actualizarUsuario() {
		
	}
	
	
	
	
}

