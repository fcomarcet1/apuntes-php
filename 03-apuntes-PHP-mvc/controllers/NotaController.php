<?php

class NotaController {
	
	public function listar() {
		
		// cargamos modelo
		require_once 'models/nota.php';
		
		//logica de la accion del controlador
		$nota = new Nota();
		
		//obtener todas las notas 
		$tabla = 'notas';
		$all_notas = $nota->conseguirTodos($tabla);
		
		//cargamos vista
		require_once 'views/nota/listar.php';
		
	}
	
	public function crear() {
		
		// cargamos modelo
		require_once 'models/nota.php';
		
		$nota = new Nota();
		$nota->setUsuario_id(1);
		$nota->setTitulo('Nota de PHP-MVC');
		$nota->setDescripcion('ESto es una descripcion nota del master en php-mvc');

		$guardar = $nota ->Guardar();
		
		//echo $nota->db->error;
		//die();
		
		//en este caso supongo que no usamos el require de la vista dado que el header ya nos lleva a 
		// una vista que contiene el require
		
		header('Location: index.php?controller=Nota&action=listar');
		
	}
	
	public function borrar() {
		
	}
}
