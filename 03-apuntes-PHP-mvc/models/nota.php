<?php

//cargamos ModeloBase para poder heredar
require_once 'ModeloBase.php';

class Nota extends ModeloBase {
	
	public $usuario_id;
	public $titulo;
	public $descripcion;
	
	// asi tenemos acceso a la propiedad heredada de ModeloBase donde tenemos la conexion a la db en el constructor
	public function __construct() {
		parent::__construct();
	}
	
	public function getUsuario_id() {
		return $this->usuario_id;
	}

	public function getTitulo() {
		return $this->titulo;
	}

	public function getDescripcion() {
		return $this->descripcion;
	}

	public function setUsuario_id($usuario_id) {
		$this->usuario_id = $usuario_id;
		return $this;
	}

	public function setTitulo($titulo) {
		$this->titulo = $titulo;
		return $this;
	}

	public function setDescripcion($descripcion) {
		$this->descripcion = $descripcion;
		return $this ;
	}

	
	public function Guardar() {
		
		$sql = "INSERT INTO notas (usuario_id, titulo, descripcion, fecha)"
				. "VALUES( {$this->usuario_id}, '{$this->titulo}', '{$this->descripcion}', CURDATE() );";
				
		$guardado = $this->db->query($sql) ;
		return $guardado;
		
	}
	
}
