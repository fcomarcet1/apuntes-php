<?php

//cargamos ModeloBase para poder heredar
require_once 'ModeloBase.php';

class Usuario extends ModeloBase {
	
	public $nombre;
	public $apellidos;
	public $email;
	public $password;
	
	
	// asi tenemos acceso a la propiedad heredada de ModeloBase donde tenemos la conexion a la db en el constructor
	public function __construct() {
		parent::__construct();
	}
	
	public function getNombre() {
		return $this->nombre;
	}

	public function getApellidos() {
		return $this->apellidos;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getPassword() {
		return $this->password;
	}

	public function setNombre($nombre) {
		$this->nombre = $nombre;
		return $this;
	}

	public function setApellidos($apellidos) {
		$this->apellidos = $apellidos;
		return $this;
	}

	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	public function setPassword($password) {
		$this->password = $password;
		return $this;
	}


	
	
	
}
