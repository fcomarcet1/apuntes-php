<?php
include_once 'config/database.php';

class ModeloBase{
	
	public $db;
	
	public function __construct() {
		// los :: es porque proviene de una clase estatica
		$this->db = Database::Conectar();
		return $this;
	}

	public function conseguirTodos($tabla) {	
		//var_dump($this->db);
		$sql = "SELECT * FROM $tabla ORDER BY id ASC";
		$query = $this->db->query($sql);
		return $query;
	}
	
}