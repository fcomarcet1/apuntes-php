<h1>Bienvenido a mi web con MVC</h1>
<?php
/*
  MODELS ->Iran los modelos que interacctuan con la BD
 *  VIEWS -> Scripts de php que van a mostrar indformacion por pantalla al usuario
 * 			(temdremos un fichero por cada accion y un directorio por cada controlador)
 * CONTROLLERS -> Iran las clases de php que van a recibir datos desde las vistas van a procesarlo 
 * 				  enviarlo al modelo y viceversa
 */

// CONTROLADOR FRONTAL.

// Cargamos controladores para acceder a sus metodos
//require_once 'controllers/UsuarioController.php';
//require_once 'controllers/NotaController.php';


//cargamos el autoload para cargar automaticamente los controladores
require_once 'autoload.php'; 

// creamos un nuevo objeto para poder cargar sus metodos
/*
  $configuracion = new UsuarioController();
  $configuracion->mostrarTodos();
  $configuracion->creaUsuario();
 */
//esta opcion no es viable ir cargando manualmente metodo por metodo

if(isset($_GET['controller'])){
	
	//como todos los controladores acaban en *Controller lo concatenamos
	$nameController = $_GET['controller'].'Controller';
} 
else {
	
	echo 'La pagina que buscas no existe';
	exit();
}

//cargar automaticamente controladores en funcion de parametros $_GET 
if ((isset($nameController)) && (class_exists($nameController))) {

	$controlador = new $nameController();

	//cargar automaticamente metodos o acciones en funcion de parametros $_GET 
	if ((isset($_GET['action'])) && (method_exists($controlador, $_GET['action']))) {

		$action = $_GET['action'];
		$controlador->$action();
	} 
	else {
		echo 'La pagina que buscas no existe';
	}
} 
else {
	echo 'La pagina que buscas no existe2';
}

