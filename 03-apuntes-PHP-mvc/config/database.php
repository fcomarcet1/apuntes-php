<?php

class Database { // clase estatica
	
	public $hostdb;
	public $userdb;
	public $password_db;
	public $database;
	public $portdb;
	
	public static function Conectar() {
		
		$hostdb = "localhost";
		$userdb = "admin";
		$password_db = "admin";
		$database = "notas_master";
		$portdb = 3306;
		
		$conexion = new mysqli($hostdb, $userdb, $password_db, $database, $portdb);
		$conexion->query("SET NAMES 'utf8'");
		
		return $conexion;

	}
}
