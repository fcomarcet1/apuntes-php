<?php

include("config.php");

$conexion = new mysqli();
$conexion->connect(DB_HOST, DB_USUARIO, DB_PASSWORD, DB_NOMBRE);

$error = $conexion->connect_errno;

if ($error == null) {

	/*
	 	El método query tiene un parámetro opcional que afecta a cómo se obtienen internamente los
		resultados, pero no a la forma de utilizarlos posteriormente. En la opción por defecto,
		MYSQLI_STORE_RESULT, los resultados se recuperan todos juntos de la base de datos y se almacenan de
		forma local. 
		Si cambiamos esta opción por el valor MYSQLI_USE_RESULT, los datos se van recuperando
		del servidor según se vayan necesitando.
	 
		$resultado = $conexion->query('SELECT producto, unidades FROM stock', MYSQLI_USE_RESULT);
	 */
	
	//$result = $conexion->query($sql);
	
	$sql = "DELETE FROM stock WHERE unidades=0";
	$result_mode = "MYSQLI_USE_RESULT";
	$result = $conexion->query($sql, $result_mode);
	
	/* validacion query */	
	if ($result) {
		$rows_affected = $conexion->affected_rows;
		echo "Se han borrado" . $rows_affected . "registros";
	}
}
else {
	echo "Error al conectar con la BD". $error;
}
/* En caso de se un consulta SELECT liberamos memoria $conexion->free(); || $result->close(); $conexion->close(); */


$conexion->close();