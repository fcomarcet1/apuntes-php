<?php

include("config.php");

$conexion = new mysqli(DB_HOST, DB_USUARIO, DB_PASSWORD);


/* Validacion conexion */ 
$error = $dwes->connect_errno;
if ($error != null) {
	echo "<p>Error $error conectando a la base de datos: $dwes->connect_error</p>";
	exit();
}

/* cambiar base de datos una vez estableida la conexion */
$dbname = "blog_master";
$conexion->select_db($dbname);