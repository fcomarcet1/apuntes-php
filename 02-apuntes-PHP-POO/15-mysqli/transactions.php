<?php

/*
 *  si necesitas utilizar transacciones deberás asegurarte de que estén
	soportadas por el motor de almacenamiento que gestiona tus tablas en MySQL. Si utilizas X, por
	defecto cada consulta individual se incluye dentro de su propia transacción. Puedes gestionar este
	comportamiento con el método autocommit (función mysqli_autocommit).
 
	$conexion->autocommit(false); // deshabilitamos el modo transaccional automático
 
    Al deshabilitar las transacciones automáticas, las siguientes operaciones sobre la base de datos
	iniciarán una transacción que deberás finalizar utilizando:
  
		 COMMIT (o la función mysqli_commit). Realizar una operación "commit" de la transacción actual,
		devolviendo true si se ha realizado correctamente o false en caso contrario.
  
		 ROLLBACK (o la función mysqli_rollback). Realizar una operación "rollback" de la transacción
		actual, devolviendo true si se ha realizado correctamente o false en caso contrario.
 */

include("config.php");

$conexion = new mysqli();
$conexion->connect(DB_HOST, DB_USUARIO, DB_PASSWORD, DB_NOMBRE);

$error = $conexion->connect_errno;

/* deshabilitamos el modo transaccional automático */


if ($error == null) {
	
	$conexion->autocommit(false);
	
	$sql_delete = "DELETE FROM stock WHERE unidades=0";
	$sql_update = "UPDATE stock SET unidades=3 WHERE producto='STYLUSSX515W' ";
	
	$result_mode = "MYSQLI_USE_RESULT";
	$result_delete = $conexion->query($sql_delete,$result_mode);
	$result_update = $conexion->query($sql_update,$result_mode);
	
	if ($result_delete) {
		$conexion->commit(); /* Confirmar cambios */
	} 
	else {
		$conexion->rollback(); /* Deshacer cambios */
	}
	
	if ($result_update) {
		$conexion->commit(); /* Confirmar cambios */
	}
	else {
		$conexion->rollback(); /* Deshacer cambios */
	}
	
	$conexion->close();
}
else{
	echo "Error al conectar con la BD". $error;
}