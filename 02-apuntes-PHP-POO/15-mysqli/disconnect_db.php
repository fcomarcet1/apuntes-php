<?php

include("config.php");

$conexion = new mysqli(DB_HOST, DB_USUARIO, DB_PASSWORD, DB_NOMBRE);
$error_msg = $conexion->connect_error;

if ($error_msg != null) {
		echo "<p>Error $error_msg conectando a la base de datos: $conexion->connect_error</p>";
		exit();
}

/* Cerrar conexion */
$conexion->close();