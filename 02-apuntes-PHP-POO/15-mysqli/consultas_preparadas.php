<?php
require_once ('config.php');

/* USADO EN CONSULTAS QUE SE VAYAN A UTILIZAR MUCHO EN LA APP
 *  Cada vez que se envía una consulta al servidor, éste debe analizarla antes de ejecutarla. Algunas
	sentencias SQL, como las que insertan valores en una tabla, deben repetirse de forma habitual en un
	programa. Para acelerar este proceso, MySQL admite consultas preparadas. Estas consultas se
	almacenan en el servidor listas para ser ejecutadas cuando sea necesario.
 
    Para trabajar con consultas preparadas con la extensión MySQLi de PHP, debes utilizar la clase
	mysqli_stmt. Utilizando el método stmt_init de la clase mysqli (o la función mysqli_stmt_init)
	obtienes un objeto de dicha clase.
  
	$conexion = new mysqli('localhost', 'dwes', 'abc123.', 'dwes');
	$query = $conexion->stmt_init();
  
	Los pasos que debes seguir para ejecutar una consulta preparada son:
		 Preparar la consulta en el servidor MySQL utilizando el método prepare (función mysqli_stmt_prepare).
		
 		 Ejecutar la consulta, tantas veces como sea necesario, con el método execute: (función mysqli_stmt_execute).
		
 		 Una vez que ya no se necesita más, se debe ejecutar el método close (función mysqli_stmt_close).
  
	Por ejemplo, para preparar y ejecutar una consulta que inserta un nuevo registro en la tabla familia:
  
	$query = $conexion->stmt_init();
	$query->prepare('INSERT INTO familia (cod, nombre) VALUES ("TABLET", "Tablet PC")');
	$query->execute();
	$query->close();
	$conexion->close();
  
	El problema que ya habrás observado, es que de poco sirve preparar una consulta de inserción de
	datos como la anterior, si los valores que inserta son siempre los mismos. 
  
    Por este motivo las consultas preparadas admiten parámetros. Para preparar una consulta con parámetros, en lugar de
	poner los valores debes indicar con un signo de interrogación su posición dentro de la sentencia SQL.
  
	$query->prepare('INSERT INTO familia (cod, nombre) VALUES (?, ?)');
 
	Y antes de ejecutar la consulta tienes que utilizar el método bind_param (o la función
	mysqli_stmt_bind_param) para sustituir cada parámetro por su valor. El primer parámetro del método
	bind_param es una cadena de texto en la que cada carácter indica el tipo de un parámetro, según la
	siguiente tabla.
  
 	$stmt = $mysqli->prepare("INSERT INTO CountryLanguage VALUES (?, ?, ?, ?)");
	$stmt->bind_param('sssd', $code, $language, $official, $percent);
  
		Carácter. Tipo del parámetro.
			I.		Número entero.
			D.		Número real (doble precisión).
			S.		Cadena de texto.
			B.		Contenido en formato binario (BLOB).
  	
 */

$conexion = new mysqli();
$conexion->connect(DB_HOST, DB_USUARIO, DB_PASSWORD, DB_NOMBRE);

/* validacion conexion */
if ($conexion->connect_error) {
    die("La conexión ha fallado " . $conexion->connect_error);
}


/* Consulta preparada */
$query = $conexion->stmt_init();

/* Añadimos los bind_param ? */
$sql = "INSERT INTO familia (cod, nombre) VALUES (?, ?)";

/* Preparacion de la consulta */
$query->prepare($sql);

$cod_producto = $_REQUEST['cod_producto'];
$nombre_producto = $_REQUEST['nombre_producto'];

/* Agrega variables a una sentencia preparada como parámetros */
$query->bind_param('ss', $cod_producto, $nombre_producto);

/* Ejecutamos consulta*/
$query->execute();

/* Cierre cpnexion */
$query->close();