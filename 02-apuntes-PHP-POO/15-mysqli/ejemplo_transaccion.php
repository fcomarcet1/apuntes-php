<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "
	http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<title>Ejercicio: Transacción con MySQLi</title>
	</head>
	<body>
		<?php
		
		@ $conexion = new mysqli("localhost", "admin", "admin.", "notas_master", 3306);
		
		$error = $conexion->connect_errno;
		
		/* validamos conexion */
		if ($error != null) {
			print "<p>Se ha producido el error: $conexion->connect_error.</p>";
			exit();
		}
		// Definimos una variable para comprobar la ejecución de las consultas
		$check_query = true;
		
		// Iniciamos la transacción
		$conexion->autocommit(false);
		
		$sql_update = 'UPDATE stock SET unidades=1 WHERE producto="3DSNG" AND tienda=1';
		$sql_insert = 'INSERT INTO `stock` (`producto`, `tienda`, `unidades`) VALUES ("3DSNG", 3, 1)';
		
		$result_mode = "MYSQLI_USE_RESULT";
		
		$result_update = $conexion->query($sql_update, $result_mode);
		$result_insert = $conexion->query($sql_insert, $result_mode);
		
		/* Inicialmente $check_query = true */
		if ($result_update != true){
			$check_query = false;
		}
		
		if ($result_insert != true){
			$check_query = false;
		}
					
		/* En funcion de la variable $check_query realizamos commit() o rollback() */
		if ($check_query == true) {
			
			$conexion->commit(); /* Confirmamos cambios */
			print "<p>Los cambios se han realizado correctamente.</p>";
		} 
		else { 
			
			$conexion->rollback(); /* deshacemos cambios */
			print "<p>No se han podido realizar los cambios.</p>";
		}
		
		/* cerramos conexion con la BD */
		$conexion->close();
		unset($conexion);
		
		?>
	</body>
</html>