<?php

include("config.php");

/*
 * $conexion = new mysqli(DB_HOST, DB_USUARIO, DB_PASSWORD, DB_NOMBRE);
 */

$conexion = new mysqli();
$conexion->connect(DB_HOST, DB_USUARIO, DB_PASSWORD, DB_NOMBRE);


//validacion conexion
if ($conexion->connect_error) {
    die("La conexión ha fallado " . $conexion->connect_error);
}

/*
	//otra manera
 
    $error_msg = $conexion->connect_error; //devuelve error
	$error_num = $conexion->connect_errno; //devuelve el nº del error
  
	if ($error_msg != null) {
		echo "<p>Error $error_msg conectando a la base de datos: $conexion->connect_error</p>";
		exit();
	}

	//otra manera
	if ($error_num != null) {
		echo "<p>Error $error_num conectando a la base de datos: $conexion->connect_errno</p>";
		exit();
	}

*/