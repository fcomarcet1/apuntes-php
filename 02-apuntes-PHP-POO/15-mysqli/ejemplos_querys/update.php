<?php
include("config/configuracion.php");

$conexion = new mysqli(DB_HOST, DB_USUARIO, DB_PASSWORD, DB_NOMBRE);

if ($conexion->connect_error) {
    die("La conexión ha fallado " . $conexion->connect_error);
}

$sql = "UPDATE usuarios SET email = ? WHERE id = ?";
$sentencia = $conexion->prepare($sql);
$sentencia->bind_param("si","nuevo@email.com", 123);
$sentencia->execute();
$sentencia->close();
$conexion->close();