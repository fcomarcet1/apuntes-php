<?php
include("config/configuracion.php");

$conexion = new mysqli(DB_HOST, DB_USUARIO, DB_PASSWORD, DB_NOMBRE);

if ($conexion->connect_error) {
    die("La conexión ha fallado " . $conexion->connect_error);
}
$sql = "INSERT INTO usuarios (nombre, apellidos, email, telefono)
VALUES (?, ?, ?, ?)";

$sentencia = $conexion->prepare($sql);
$sentencia->bind_param("ssss","Santiago", "Faci", "email", 123456789);

$sentencia->execute();
$sentencia->close();
$conexion->close();