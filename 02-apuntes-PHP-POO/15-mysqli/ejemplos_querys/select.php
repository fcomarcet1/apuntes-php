<?php
include("config/configuracion.php");

$conexion = new mysqli(DB_HOST, DB_USUARIO, DB_PASSWORD, DB_NOMBRE);

if ($conexion->connect_error) {
    die("La conexión ha fallado " . $conexion->connect_error);
}

$sql = "SELECT * FROM usuarios";
$sentencia = $conexion->prepare($sql);
$sentencia->execute();
$resultado = $sentencia->get_result();

while ($fila = $resultado->fetch_assoc()) {

    echo "<li>" . $fila["nombre"] . "</li>";
}

$sentencia->close();
$conexion->close();