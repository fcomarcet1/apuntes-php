<?php 

/*
debemos usar las consultas parametrizadas de forma que una vez “preparada” la consulta podamos inyectarle nosotros los parámetros que se han indicado con el caracter ?. Al asociaar los parámetros con sus valores, utilizando la llamada al método bind_param asociaremos cada parámetro con un valor, indicando ademas el tipo del parámetro con los siguientes caracteres:
    s: Si es una cadena (string)
    d: Si es un número con decimales (double)
    i: Si es un número entero o booleano (integer)

*/

include("config/configuracion.php");

$conexion = new mysqli(DB_HOST, DB_USUARIO, DB_PASSWORD, DB_NOMBRE);

if ($conexion->connect_error) {
    die("La conexión ha fallado " . $conexion->connect_error);
}

$sql = "SELECT * FROM usuarios WHERE id = ?";
$sentencia = $conexion->prepare($sql);
$sentencia->bind_param("i",12);
$sentencia->execute();
$resultado = $sentencia->get_result();

while ($fila = $resultado->fetch_row(MYSQLI_ASSOC)) {
    echo "<li>" . $fila["nombre"] . "</li>";
}

$sentencia->close();
$conexion->close();