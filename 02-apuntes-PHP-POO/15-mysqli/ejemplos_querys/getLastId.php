<?php 
include("config/configuracion.php");

$conexion = new mysqli(DB_HOST, DB_USUARIO, DB_PASSWORD, DB_NOMBRE);

if ($conexion->connect_error) {
    die("La conexión ha fallado " . $conexion->connect_error);
}

$sql = "INSERT INTO pedidos (descripcion, precio, fecha_pedido)
VALUES (?, ?, ?, ?)";
$sentencia = $conexion->prepare($sql);
$sentencia->bind_param("sds","Pedido del día", 120, "2017-11-21");
$sentencia->execute();

$id_pedido = $conexion->insert_id;

while ($detalles as $detalle) {
    
    $sql = "INSERT INTO detalles (id_pedido, id_articulo, precio) VALUES " .
    "(?, ?, ?)";
    $sentencia = $conexion->prepare($sql);
    $sentencia->bind_param("iid", $id_pedido, $detalle->id_articulo, $detalle->precio);
    $sentencia->execute();
}