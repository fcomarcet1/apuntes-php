<?php

class Persona {

	public $nombre;
	private $edad;
	protected $pasaporte;
	public static $web;

	public function __construct($nombre, $edad, $pasaporte) {
		$this->nombre = $nombre;
		$this->edad = $edad;
		$this->pasaporte = $pasaporte;
	}

	public function getNombre() {
		return $this->nombre;
	}

	public function getEdad() {
		return $this->edad;
	}

	public function getPasaporte() {
		return $this->pasaporte;
	}

	public static function getWeb() {
		return self::$web;
	}

	public function setNombre($nombre) {
		$this->nombre = $nombre;
		return $this;
	}

	public function setEdad($edad) {
		$this->edad = $edad;
		return $this;
	}

	public function setPasaporte($pasaporte) {
		$this->pasaporte = $pasaporte;
		return $this;
	}

	public static function setWeb($web) {
		self::$web = $web;
		return self;
	}

}

//---------------------------------------------------
class PersonaAPositivo extends Persona {

	public static $tipo_sangre = 'A+';

}

//-----------------------------------------------------


$persona = new Persona();
$personaA = new PersonaAPositivo();

/* DENTRO DE LA CLASE
 * Acceso a variables desde el ámbito de la clase:
 * 
 * 			return $this->nombre;
 * 
 * Si es STATIC:
 * 
 * 		self::$variable_estatica_de_esta_clase;
 * 		parent::$variable_estatica_de_clase_madre;
 */

/* FUERA DE LA CLASE
 * Acceso a variables NO STATIC:
 * 		$objeto->variable
 * 
 */

# accedo a la variable estática (podemos acceder incluso antes de instanciar un nuevo objeto)
print PersonaAPositivo::$tipo_sangre;

# creo el objeto instanciando la clase
$persona_a_positivo = new PersonaAPositivo();

# accedo a la variable NO estática
print $persona_a_positivo->nombre;

# accedo a la variable estática
print PersonaAPositivo::$tipo_sangre;


/*
 * CONSTRUCTOR
 * El método __construct() es aquel que será invocado de manera automática, al
 * instanciar un objeto. Su función es la de ejecutar cualquier inicialización que el objeto
 * necesite antes de ser utilizado.
 */

//CONSTRUCTOR CONVENCIONAL
class Persona3 {

	public $nombre;
	private $edad;
	protected $pasaporte;
	public static $web;

	public function __construct($nombre, $edad, $pasaporte) {
		$this->nombre = $nombre;
		$this->edad = $edad;
		$this->pasaporte = $pasaporte;
	}

}

//OJO ESTE EJEMPLO QUE EL CONSTRUCTOR COGE UNA VARIABLE DE UN METODO

class Producto {
#defino algunas propiedades

	public $nombre;
	public $precio;
	protected $estado;

#defino el método set_estado_producto()

	protected function set_estado_producto($estado) {
		$this->estado = $estado;
	}

# constructor de la clase

	function __construct() {
		$this->set_estado_producto('en uso');
	}

}

/*
 * DESTRUCTOR
 * El método __destruct() es el encargado de liberar de la memoria, al objeto cuando ya
 * no es referenciado. Se puede aprovechar este método, para realizar otras tareas que se
 * estimen necesarias al momento de destruir un objeto.
 */

# declaro la clase

class Producto2 {
	
#defino algunas propiedades
	public $nombre;
	public $precio;
	protected $estado;

#defino el método set_estado_producto()
	protected function set_estado_producto($estado) {
		$this->estado = $estado;
	}

# constructor de la clase
	function __construct() {
		$this->set_estado_producto('en uso');
	}

# destructor de la clase
	function __destruct() {
		$this->set_estado_producto('liberado');
		print 'El objeto ha sido destruido';
	}

}
