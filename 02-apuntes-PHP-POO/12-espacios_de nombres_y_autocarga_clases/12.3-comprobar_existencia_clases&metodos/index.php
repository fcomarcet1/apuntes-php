<?php

//ESPACIOS DE NOMBRES

//cargar espacio de nombres
use Misclases\Usuario;
use Misclases\Categoria;
use Misclases\Entrada;
use PanelAdministrador\Usuario as Usuario_PanelAdmin;

/*  SIMILAR
	use Misclases\Usuario, Misclases\Categoria, Misclases\Entrada; 
 */


require_once 'autoload.php';

class Principal {
	
	public $usuario;
	public $entrada;
	public $categoria;
	
	public function __construct() {
		
		//le damos a usuario los valores del objeto usuario
		$this->usuario = new Usuario();
		$this->categoria = new Categoria();
		$this->entrada = new Entrada();
	}
	
	public function getUsuario() {
		return $this->usuario;
	}

	public function getEntrada() {
		return $this->entrada;
	}

	public function getCategoria() {
		return $this->categoria;
	}

	public function setUsuario($usuario) {
		$this->usuario = $usuario;
		return $this;
	}

	public function setEntrada($entrada) {
		$this->entrada = $entrada;
		return $this;
	}

	public function setCategoria($categoria) {
		$this->categoria = $categoria;
		return $this;
	}
	
	//contsntes para clases
	public function informacion() {
		echo __CLASS__;
		echo __FILE__;
		echo __NAMESPACE__;
	}


}

//Objeto principal
$principal = new Principal();
var_dump($principal->usuario);

//Objeto otro paquete
$usuario = new Usuario_PanelAdmin();
var_dump($usuario);

//COMPROBAR EXISTENCIA CLASES Y METODOS

//Comprobar si existe una clase
$clase = class_exists('Misclases\usuario'); //ok

if($clase){
	echo 'La clase existe';
} 
else {
	echo 'La clase NO existe';
}

//con @ no devuelve los warnings
$clase2 = @class_exists('PanelAdministrador\Usuario');//ok

if($clase2){
	echo 'La clase existe';
} 
else {
	echo 'La clase NO existe';
}


//averiguar metodos de una clase
$principal2 = new Principal();
var_dump(get_class_methods($principal2));

$metodos = get_class_methods($principal2);
$busqueda = array_search('setvelocidad', $metodos);
var_dump($busqueda);