<?php

namespace Misclases;

class Categoria {
	
	public $nombre;
	public $descripcion;
	
	public function __construct() {
		
		$this->nombre = "Rol";
		$this->descripcion = "Categoria enfocada a la review de videojuegos de rol.";
	}

	
}