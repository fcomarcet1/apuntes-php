<?php

//ESPACIOS DE NOMBRES

//cargar espacio de nombres
use Misclases\Usuario;
use Misclases\Categoria;
use Misclases\Entrada;
use PanelAdministrador\Usuario as Usuario_PanelAdmin;

/*  SIMILAR
	use Misclases\Usuario, Misclases\Categoria, Misclases\Entrada; 
 */


require_once 'autoload.php';

class Principal {
	
	public $usuario;
	public $entrada;
	public $categoria;
	
	public function __construct() {
		
		//le damos a usuario los valores del objeto usuario
		$this->usuario = new Usuario();
		$this->categoria = new Categoria();
		$this->entrada = new Entrada();
			
	}
}

//Objeto principal
$principal = new Principal();
var_dump($principal->usuario);

//Objeto otro paquete
$usuario = new Usuario_PanelAdmin();
var_dump($usuario);


