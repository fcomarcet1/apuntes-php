<?php

class Usuario {
	
	const URL_COMPLETA= "http://localhost";
	public $email;
	public $password;
	
	public function getEmail() {
		return $this->email;
	}

	public function getPassword() {
		return $this->password;
	}

	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	public function setPassword($password) {
		$this->password = $password;
		return $this;
	}
	
}

//Al ser una propiedad estatica se puede acceder sin crear un nuevo objeto 
echo Usuario::URL_COMPLETA;

//Vamos a crear un nuevo objeto y ver si aparece la cte
$usuario = new Usuario();

//Si hacemos un var_dump vemos como NO aparece la contante
var_dump($usuario);

//para acceder a la constante;
echo Usuario::URL_COMPLETA;

