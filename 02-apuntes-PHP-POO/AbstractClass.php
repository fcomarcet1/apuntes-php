

<?php

abstract class ProtectVis {

	abstract protected function countMoney();
	abstract protected function countMods();

	protected $wage;

	protected function setHourly($hourly) {
		$money = $hourly;
		return $money;
	}

}

class ConcreteProtect extends ProtectVis {

	function __construct() {
		$this->countMoney();
	}

	protected function countMoney() {
		echo "ok";
	}

	protected function countMods($param) {
		echo "ok";
	}
}
