<?php 

/* mysqli->fetch_assoc — Obtiene una fila del resultado como una matriz asociativa */

/* Example#2 Estilo por procedimientos */
$link = mysqli_connect("localhost", "my_user", "my_password", "world");

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$query = "SELECT Name, CountryCode FROM City ORDER by ID DESC LIMIT 50,5";

if ($result = mysqli_query($link, $query)) {

    /* fetch associative array */
    while ($row = mysqli_fetch_assoc($result)) {
        printf ("%s (%s)\n", $row["Name"], $row["CountryCode"]);
    }

    /* free result set */
    mysqli_free_result($result);
}

/* close connection */
mysqli_close($link);


/* 
 * El resultado del ejemplo seria:
		Pueblo (USA)
		Arvada (USA)
		Cape Coral (USA)
		Green Bay (USA)
		Santa Clara (USA)
*/

//--------------------------------------------------------
//--------------------------------------------------------

// Example#1 Estilo orientado a objetos

$mysqli = new mysqli("localhost", "my_user", "my_password", "world");

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
 
$query = "SELECT Name, CountryCode FROM City ORDER by ID DESC LIMIT 50,5";

if ($result = $mysqli->query($query)) {

    /* fetch associative array */
    while ($row = $result->fetch_assoc()) {
        printf ("%s (%s)\n", $row["Name"], $row["CountryCode"]);
    }

    /* free result set */
    $result->close();
}

/* close connection */
$mysqli->close();

/* 
 El resultado del ejemplo seria:
		Pueblo (USA)
		Arvada (USA)
		Cape Coral (USA)
		Green Bay (USA)
		Santa Clara (USA)

 */

?>    










?>