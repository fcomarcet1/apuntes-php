<?php

require_once 'Persona4.php';

$persona = new Persona4();

$propiedades = get_class_vars(get_class($persona));

var_dump($persona);
var_dump($propiedades); // solo vemos las public

echo '<hr/>';
echo "<h3>Recorremos objeto con foreach(array as value) pero solo visualizamoss atributos public </h3>";
foreach ($persona as $value) {
    echo $value . "<br/>";
}
 
echo '<hr/>';

echo "<h3>Recorremos objeto foreach(array as key=>value) pero solo visualizamoss atributos public </h3>";
foreach ($persona as $key => $value) {
    echo $key . "|" . $value . "<br/>";
} // solo nos esta imprimendo provincia, pais, e importe ya que nombre y apellidos son private

echo '<hr/>';
echo "<h3>Recorremos objeto desde un metodo de la propia clase y podemos visualizar todos los atributos incluidos los private</h3>";
echo $persona->getPropiedades();

 function test (){
    return "test";
}

class Pepe{
    public $nombre;

    public function __construct(Type $var = null) {
        $this->var = $var;
    }
}