<?php

class Persona4 {

	// Propiedades
	private $nombre = "Pepe";
	private $apellidos = "Lotas";
	public $provincia = "Benilloba";
	public $pais = "ESPAÑA";
	protected $importe = 100;

	// Constantes:
	const PERSONA_HOMBRE = "HOMBRE";
	const PERSONA_MUJER = "MUJER";

	// Constructor vacío:
	function __construct() {
		
	}

	// Métodos:
	function getNombre() {
		return $this->nombre;
	}

	function setNombre($nombre) {
		$this->nombre = $nombre;
	}

	private function getPais() {
		return $this->pais;
	}

	public function getPropiedades() {
		
		$aPropiedades1 = get_class_vars("Persona4");
		
		foreach ($aPropiedades1 as $nombre => $valor) {
			echo $nombre . ": [" . $valor . "]<br/>";
		}
	}

}

?>