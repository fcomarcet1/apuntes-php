<?php
//HERENCIA: Posibilidad de compartir atributos y metodos entre distintas clases


class Persona{
	
	public $nombre;
	public $apellidos;
	public $altura;
	public $edad;
	

	//metodos
	public function getNombre() {
		return $this->nombre;
	}

	public function getApellidos() {
		return $this->apellidos;
	}

	public function getAltura() {
		return $this->altura;
	}

	public function getEdad() {
		return $this->edad;
	}

	public function setNombre($nombre) {
		$this->nombre = $nombre;
		return $this;
	}

	public function setApellidos($apellidos) {
		$this->apellidos = $apellidos;
		return $this;
	}

	public function setAltura($altura) {
		$this->altura = $altura;
		return $this;
	}

	public function setEdad($edad) {
		$this->edad = $edad;
		return $this;
	}

	public function hablar() {
		return "Estoy hablando";
	}

	public function caminar() {
		return "Estoy caminando";
	}
	
	
}// Fin class persona

class Informatico extends Persona{
	
	public $lenguajes;
	public $ExperienciaProgramando;
	
	public function __construct() {
		
		$this->lenguajes = "HTML, CSS, PHP";
		$this->ExperienciaProgramando = 2;
	}
	
	
	public function sabeLenguajes($lenguajes) {
		
		$this->lenguajes = $lenguajes;
		return "Se programar en:".$this->lenguajes;
		
	}

	public function programar() {
		return "Estoy programando";
	}
	
	public function repararPc() {
		return "Estoy reparando un Pc";
	}
	
	public function hacerOfimatica() {
		return "Estoy escribiendo en word";
	}
	
}

class TecnicoRedes extends Informatico{
	
	public $auditarRedes;
	public $ExperienciaRedes;
	
	public function __construct() {
		
		//para heredar del contructor padre
		parent::__construct();
		
		$this->auditarRedes = "experto";
		$this->ExperienciaRedes = 5;
		
	}

	
	public function auditoria() {
		return "Estoy auditando la red";
	}
	
}