<?php

/* result->fetch_array — Obtiene una fila como una matriz asociativa, una matriz numérica o ambos
 * 
 * El parámetro opcional tipo_de_resultado es una constante que indica qué tipo de matriz debe ser producido 
 * para la fila de datos actual. 
 * 
 * Los posibles valires para este parámetro son las constantes MYSQLI_ASSOC, MYSQLI_NUM, o MYSQLI_BOTH. 
 * Por defecto la función mysqli_fetch_array() asumirá el valor de MYSQLI_BOTH.  
 * 
 * Al usar la constante MYSQLI_ASSOC, esta función se comportará identica a la función mysqli_fetch_assoc()
 *  
 */



//Example#1 Estilo orientado a objetos

$mysqli = new mysqli("localhost", "my_user", "my_password", "world");

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
 
$query = "SELECT Name, CountryCode FROM City ORDER by ID LIMIT 3";
$result = $mysqli->query($query);

/* numeric array */
$row = $result->fetch_array(MYSQLI_NUM);
printf ("%s (%s)\n", $row[0], $row[1]);  

/* associative array */
$row = $result->fetch_array(MYSQLI_ASSOC);
printf ("%s (%s)\n", $row["Name"], $row["CountryCode"]);  

/* associative and numeric array */
$row = $result->fetch_array(MYSQLI_BOTH);
printf ("%s (%s)\n", $row[0], $row["CountryCode"]);  

/* free result set */
$result->close();

/* close connection */
$mysqli->close();

?>

<?php
 //Example#2 Estilo por procedimientos

$link = mysqli_connect("localhost", "my_user", "my_password", "world");

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$query = "SELECT Name, CountryCode FROM City ORDER by ID LIMIT 3";
$result = mysqli_query($link, $query);

/* numeric array */
$row = mysqli_fetch_array($result, MYSQLI_NUM);
printf ("%s (%s)\n", $row[0], $row[1]);  

/* associative array */
$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
printf ("%s (%s)\n", $row["Name"], $row["CountryCode"]);  

/* associative and numeric array */
$row = mysqli_fetch_array($result, MYSQLI_BOTH);
printf ("%s (%s)\n", $row[0], $row["CountryCode"]);  

/* free result set */
mysqli_free_result($result);

/* close connection */
mysqli_close($link);

//El resultado del ejemplo seria:
//
//	Kabul (AFG)
//	Qandahar (AFG)
//	Herat (AFG)

?>



    