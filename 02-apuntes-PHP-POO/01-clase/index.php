<?php
// CLASE
// Estructura para crear mas objetos de tipo coche con similares caracteristicas
	//Definir clase --> class

	class Coche{
		
		//Atributos o propiedades(variables)
		public $marca = "ferrari";
		public $color = "rojo";
		public $modelo;
		public $tipo_motor;
		public $plazas = 2;
		public $velocidad = 300;
		
		//Metodos: Son acciones que puede realizar el objeto(funciones)
		public function getColor() {
			//Con $this-> accedemos a las propiedades del metodo
			return $this->color;
		}
		
		public function setColor($color) {
			//Con $this-> accedemos a las propiedades del metodo
			 $this->color = $color;
		}
		
		public function setMarca($marca) {
			//Con $this-> accedemos a las propiedades del metodo
			 $this->marca = $marca;
		}
		
		public function acelerar() {
			
			 $this->velocidad++;
		}
		
		public function frenar() {
			
			 $this->velocidad--;
		}
		
		public function getVelocidad() {
			
			return $this->velocidad;	
		}
		
	}//FIN DEFINICION  CLASE
	
	//CREAR OBJETO O INSTANCIAR CLASE.
	$coche = new Coche();
	var_dump($coche);
	
	//USAR METODOS
	//obtener la velocidad
	echo $coche->getVelocidad();
	echo "<br/>";
	
	//obtener color
	echo $coche->setColor('Negro');
	echo "el color del cohe es:" . $coche->getColor()."<br/>";
	
	
	$coche->acelerar();
	$coche->acelerar();
	$coche->acelerar();
	$coche->acelerar();
	$coche->frenar();
	
	echo "la velocidad del coche es:".$coche->getVelocidad()."<br/>";
	
	//podemos crear mas objetos de la clase coche
	
	$coche2 = new Coche();
	
	$coche2->setMarca('Mercedes-Benz');
	
	var_dump($coche2);
	var_dump($coche);