<?php

/* 
 * result->fetch_object — Regresa la fila actual del resultado como un objeto
 * La función mysqli_fetch_object() regresará la fila actual en el resultado como un objeto donde los atributos 
 * del objeto representan los nombre de los campos encontrados dentro del resultado. 
 * 
 * Si no hay más filas en el resultado, se regresa NULL
 */

?>

<!-- Example#1 Estilo orientado a objetos -->
<?php
$mysqli = new mysqli("localhost", "my_user", "my_password", "world");

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
 
$query = "SELECT Name, CountryCode FROM City ORDER by ID DESC LIMIT 50,5";

if ($result = $mysqli->query($query)) {

    /* fetch object array */
    while ($obj = $result->fetch_object()) {
        printf ("%s (%s)\n", $obj->Name, $obj->CountryCode);
    }

    /* free result set */
    $result->close();
}

/* close connection */
$mysqli->close();
?>

<!-- Example#2 Estilo por procedimientos -->
<?php
$link = mysqli_connect("localhost", "my_user", "my_password", "world");

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$query = "SELECT Name, CountryCode FROM City ORDER by ID DESC LIMIT 50,5";

if ($result = mysqli_query($link, $query)) {

    /* fetch associative array */
    while ($obj = mysqli_fetch_object($result)) {
        printf ("%s (%s)\n", $obj->Name, $obj->CountryCode);
    }

    /* free result set */
    mysqli_free_result($result);
}

/* close connection */
mysqli_close($link);
?>
<!--
El resultado del ejemplo seria:

	Pueblo (USA)
	Arvada (USA)
	Cape Coral (USA)
	Green Bay (USA)
	Santa Clara (USA)

-->
    