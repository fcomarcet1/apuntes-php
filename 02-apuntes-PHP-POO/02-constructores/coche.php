<?php

class Coche{
		
	//Atributos o propiedades(variables)
	//conviene darle valores dentro del constructor
	public $marca ;
	public $color ;
	public $modelo;
	public $tipo_motor;
	public $plazas ;
	public $velocidad  ;
	
	//CONSTRUCTOR
	public function __construct($color, $marca, $plazas, $velocidad) {
		$this->color = $color ;
		$this->marca = $marca ;
		$this->plazas = $plazas ; 
		$this->velocidad = $velocidad ;
		
	}

	//Metodos: Son acciones que puede realizar el objeto(funciones)
	public function getColor() {
		//Con $this-> accedemos a las propiedades del metodo
		return $this->color;
	}

	public function setColor($color) {
		//Con $this-> accedemos a las propiedades del metodo
		 $this->color = $color;
	}

	public function setMarca($marca) {
		//Con $this-> accedemos a las propiedades del metodo
		 $this->marca = $marca;
	}

	public function acelerar() {

		 $this->velocidad++;
	}

	public function frenar() {

		 $this->velocidad--;
	}

	public function getVelocidad() {

		return $this->velocidad;	
	}
		
}//FIN DEFINICION  CLASE
