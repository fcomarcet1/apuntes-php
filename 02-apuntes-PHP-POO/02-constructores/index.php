<?php
require_once 'coche.php';

$coche = new Coche("Verde","Renault", 4, 250);
$coche2 = new Coche("Blanco","Citroen", 4, 250);
$coche3 = new Coche("Gris","Ford", 4, 250);
var_dump($coche);

//si las propiedades son publicas podemos acceder
//a las propiedades sin crear un nuevo objeto

$coche->color = "Azul";

var_dump($coche);
var_dump($coche2);
var_dump($coche3);

