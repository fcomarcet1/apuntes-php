<?php

include_once 'configuracion.php';


//Para acceder a cualquier metodo de la clase estatica no es necesario instanciar un nuevo objeto.

Configuracion::setColor("Blue");
Configuracion::setEntorno("localhost");
Configuracion::setNewsletter(true);

echo Configuracion::$color.'<br/>';
echo Configuracion::$entorno.'<br/>';
echo Configuracion::$newsletter.'<br/>';

//aqui creamos un nuevo objeto poro no es necesario somo como ejemplo
$configuracion = new Configuracion();
$configuracion::$color="Red";

 // echo $configuracion->$color;  ASI NO FUNCIONA SIEMPRE ACCEDER A PROPIEDADES ESTATICAS CON ::
echo $configuracion::$color ;



