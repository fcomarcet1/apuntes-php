<?php

interface Ordenador {
	
	public function encender();
	public function apagar();
	public function reiniciar();
	public function desfragmentar();
	public function detectarUSB();
}


class iMac{
	
	private $modelo;
	
	public function getModelo() {
		return $this->modelo;
	}

	public function setModelo($modelo) {
		$this->modelo = $modelo;
		return $this;
	}
	
}

$mac = new iMac();
$mac->setModelo("MacPRO 2021");
echo $mac->getModelo();


//ahora vamos a implementar una clase en la cual se implemente la interface y vemos como es obligatorio
// definir los metodos que vienen definidos en la interfaz.

class Msi implements Ordenador{
	
	private $modelo;
	
	public function getModelo() {
		return $this->modelo;
	}

	public function setModelo($modelo) {
		$this->modelo = $modelo;
		return $this;
	}
	
	//necesitamos implementar los metodos de la interface
	public function encender() {
		return "kdalkdjals";
	}
	
	public function apagar() {
		return "kdalkdjals";
	}
	
	public function reiniciar() {
		return "kdalkdjals";
	}
	
	public function desfragmentar() {
		return "kdalkdjals";
	}
	
	public function detectarUSB() {
		return "kdalkdjals";
	}
	
}