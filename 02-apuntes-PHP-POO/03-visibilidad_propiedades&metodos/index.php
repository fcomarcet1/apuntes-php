<?php
include_once 'coche.php';

$vehiculo = new Coche("Verde","Seat", 4, 250);

/*
 * public $marca ;
 * protected $color ;
	private $modelo;
 * */

$vehiculo->marca = "Dacia";

//para modificar desde fuera un protectec debe existir un metodo que lo permita
$vehiculo->setColor("rosa");

//de la misma manera para los private
var_dump($vehiculo->modelo);//NO FUNCIONA
//usamos el metodo para acceder
$vehiculo->getModelo();


