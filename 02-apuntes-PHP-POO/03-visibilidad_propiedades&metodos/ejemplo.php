<?php
// Definimos la clase
class Coche{
     
    /* Atributos protected accesibles en clases que hereden de esta */
    protected $modelo;
    protected $color;
    protected $velocidad;
     
    /* Constructor, siempre tiene que ser publico */
    public function __construct($modelo, $color, $velocidad = 0){
        $this->modelo = $modelo;
        $this->color = $color;
        $this->velocidad = $velocidad;
    }
     
    /* Métodos privados solo se pueden llamar dentro esta clase */
    private function getColor(){
        return $this->color;
    }
     
    private function setColor($color){
        $this->color = $color;
    }
     
    private function acelerar(){
        $this->velocidad++;
    }
     
    private function frenar(){
        $this->velocidad--;
    }
     
    private function getVelocidad(){
        return $this->velocidad;
    }
     
    public function mostrarInfo(){
         
        // Llamamos a otros métodos
        $info = "<h1>Información del coche:</h1>";
        $info.= "Modelo: ".$this->modelo;
        $info.= "<br/> Color: ".$this->getColor();
        $info.= "<br/> Velocidad: ".$this->getVelocidad();
         
        return $info;
    }
	
	
	/*
	 * Una vez que creemos el objeto coche solo podremos llamar al método mostrarInfo, 
	 * el resto de métodos son internos de esa clase porque son privados. Si intentáramos 
	 * llamar al método getVelocidad por ejemplo, php nos lanzará una error.
	 */
}