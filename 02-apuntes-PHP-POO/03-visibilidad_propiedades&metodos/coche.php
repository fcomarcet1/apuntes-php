<?php

class Coche{
		
	//PUBLIC:Podemos acceder desde cualquier lugar, dentro de la clase actual,
	//		dentro de clases heredadas, o fuera de la clase.	
	public $marca ;
	//PROTECTED:Podemos accder desde la clase que los define, y las clases que hereden
	//			de esta clase			
	protected $color ;
	//PRIVATE:unicamente se puede acceder desde la clase que la define.
	private $modelo;
	
	public $tipo_motor;
	public $plazas ;
	public $velocidad  ;
	
	//CONSTRUCTOR
	public function __construct($color, $marca, $plazas, $velocidad) {
		$this->color = $color ;
		$this->marca = $marca ;
		$this->plazas = $plazas ; 
		$this->velocidad = $velocidad ;
		
	}

	//Metodos: Son acciones que puede realizar el objeto(funciones)
	public function getColor() {
		//Con $this-> accedemos a las propiedades del metodo
		return $this->color;
	}

	public function setColor($color) {
		//Con $this-> accedemos a las propiedades del metodo
		 $this->color = $color;
	}

	public function setMarca($marca) {
		//Con $this-> accedemos a las propiedades del metodo
		 $this->marca = $marca;
	}

	public function acelerar() {

		 $this->velocidad++;
	}

	public function frenar() {

		 $this->velocidad--;
	}

	public function getVelocidad() {

		return $this->velocidad;	
	}
	
	public function getModelo() {

		return $this->modelo;	
	}
		
}//FIN DEFINICION  CLASE
