<?php
//CLASSES ABSTRACTAS
/*
En programación, se habla de «abstracción» para referirse a elementos que definen comportamientos 
independientemente de su concreción. Por ejemplo, pensemos en un algoritmo que sirve para apagar y encender.

	abstracción de interruptor {

	if ( estado == 0 ) {

		apaga;

	} else if ( estado == 1 )

		enciende;

	}

Este algoritmo interruptor se limita a comprobar un estado y, en función de eso, apagar o encender. Es un modelo abstracto que se puede emplear, concretar, en una radio, un submarino, la luz de una biblioteca o una lavadora... en fin, en cualquier cosa que se apague y se encienda.
Una clase abstracta es una clase que no puede ser instanciada. 
 * 
Las clases abstractas pueden definir métodos abstractos, que son métodos sin cuerpo, solo una definición:

	abstract class MyAbstractClass {
 
		 abstract public function doSomething($a, $b);
	}
  
  
  
  
abstract class SoyUnaClaseAbstracta {

	abstract protected function soyUnMetodoAbstracto();

}

class hija extends SoyUnaClaseAbstracta {

	// Esto no funciona
	private function soyUnMetodoAbstracto() {

	}

	// Esto sí funciona
	public function soyUnMetodoAbstracto() {
 * 
			definimos funcionalidad
	}

}

 *  */
//RESUMEN: En una clase abstracta puedo definir cierta funcionalidad , ciertos metodos los cuales heredar
//		   ademas de poder definir metodos abstractos que en un inicio no se sabe que funcionalidad van a 
//		   tener dentro de la clase hija, como el ejemplo del interruptor de encendido/apagado si se usara 
//		   en una radio, un barco una luz etc.
//		   Es obligatorio que se definan en la clase hija el metodo y su funcionalidad
//		   	


abstract class Ordenador {
	
	public $encendido;
	
	abstract public function encender(); 
	
	public function apagar() {
		$this->encendido = false;
	}
	
}

class PcAsus  extends Ordenador{
	public $software;
	
	public function arrancarSoftware() {
		$this->software = true;
	}
	
	public function encender() {
		$this->encendido = true;
		
	}
}

$ordenador = new PcAsus();
var_dump($ordenador);

$ordenador->arrancarSoftware();
$ordenador->apagar();