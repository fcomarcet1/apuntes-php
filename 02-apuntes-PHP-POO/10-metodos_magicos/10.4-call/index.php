<?php

class Persona{
	
	private $nombre;
	private $edad;
	private $ciudad;
	
	public function __construct($nombre, $edad, $ciudad) {
		
		$this->nombre = $nombre;
		$this->edad = $edad;
		$this->ciudad = $ciudad;
	}
	public function __call($name, $arguments) {
		
		$prefix_metodo = substr($name, 0, 3);
		
		if($prefix_metodo == 'get'){
			
			$nombre_metodo = substr(strtolower($name), 3);
			
			if(!isset($this->$nombre_metodo)){
				
				return "El método que estas invocando no existe!!";
			}
			// $this->nombre
			return $this->$nombre_metodo;
		}
		else {
			return "El método que estas invocando no existe!!";
		}
	}
		
}

$persona = new Persona("Fco", 20, "Alicante");

var_dump($persona);
//si intentamos acceder a un metodo que todavia no existe nos da error evidentemente
 echo $persona->setNombre();

//podemos implementar el metodo  __call
echo $persona->getEdad();
echo $persona->getCiudad();
echo $persona->getHOLA();