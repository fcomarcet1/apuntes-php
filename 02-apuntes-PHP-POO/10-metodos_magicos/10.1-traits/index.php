<?php
/*
	TRAITS:Nos permite definir una serie de metodos que podran ser utilizados en diferentes clases.

*/
trait utilidades {
	
	public function MostrarNombre() {
		
		echo "<h1>El nombre es:" .$this->nombre."</h1>";
	}
   
}

//si queremos usar el trait -> use Nombre_trait en una clase.

class Coche {
	
	public $nombre;
	public $marca;
	
	use utilidades;
}


class Persona {
	
	public $nombre;
	public $apellidos;
	
	use utilidades;
}


class Videojuego {
	
	public $nombre;
	public $year;
	
	use utilidades;
}


//creamos una nueva instancia de cada clase


$coche = new Coche();
$coche->nombre = "Ferrari 488 GTE";
var_dump($coche);
$coche->MostrarNombre();


$persona = new Persona();
$persona->nombre = "Pepe Lotas";
$persona->MostrarNombre();

$videojuego = new Videojuego();
$videojuego->nombre = "Mario Bros";
$videojuego->MostrarNombre();
		
		