<?php

//Veamos como se comporta un objeto cuando lo tratamos como un string

class Usuario {
	
	public $nombre;
	public $email;
	
	public function __construct() {
		
		$this->nombre = "Fco";
		$this->email = "mail@mail.es";
		
		//ojo no imprimir nada en el contructor aqui es solo con fines didacticos.
		echo "creando el objeto "."<br/>";	
	}
	
	public function __destruct() {
		
		echo "<br/>Destruyendo el objeto";	
	}
	
	public function __toString() {
		
		return "Hola:{$this->nombre}, estas registrado con el email:{$this->email}";
		
	}
}


$usuario = new Usuario();

//si tratamos de imprimir el nombre no hay problema
echo $usuario->nombre ."<br/>";

//pero si intentamos imprimir el objeto $usuario
echo $usuario;// Sale EEROR que no puede ser convertido a string


//hemos de añadir un  metodo magico __toString que nos definira lo que se imprime como string

