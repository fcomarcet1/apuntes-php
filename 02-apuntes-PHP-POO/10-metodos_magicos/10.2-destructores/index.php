<?php
 
class Usuario {
	
	public $nombre;
	public $email;
	
	public function __construct() {
		
		//ojo no imprimir nada en el contructor aqui es solo con fines didacticos.
		echo "creando el objeto "."<br/>";	
	}
	
	public function __destruct() {
		
		echo "Destruyendo el objeto";	
	}
}


$usuario = new Usuario();

for ($i = 0; $i <= 50; $i++) {
	
	echo $i ."<br/>";
}


//en la salida por pantalla vemos como primero crea el objeto, hace el bucle hasta 50 y luego cuando no va a usar
// mas el objeto lo destruye