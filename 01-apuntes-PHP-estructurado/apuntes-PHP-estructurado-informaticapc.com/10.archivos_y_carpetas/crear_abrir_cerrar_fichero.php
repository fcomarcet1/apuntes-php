<?php 
/*
Para crear un archivo en PHP usaremos la función fopen() indicando el nombre del mismo como primer 
parámetro (pudiendo indicar la ruta) y el modo de creación en el segundo, pudiendo ser:

r: abre el archivo en modo sólo lectura, colocando el puntero del archivo al principio del mismo.

r+: apertura en modo lectura y escritura, colocando el puntero del archivo al principio del mismo.

w: apertura del archivo en modo sólo escritura, colocando el puntero del archivo al principio del mismo: 
    si el archivo no existe se creará, y si existe su contenido será borrado.

w+: Lo mismo que el anterior pero abre el archivo en modo lectura y escritura.

a: abre el archivo en modo sólo escritura, colocando el puntero del archivo al final del mismo: 
    si el archivo no existe será creado.

a+: Lo mismo que el anterior pero abre el archivo en modo lectura y escritura.

x: abre el archivo en modo sólo escritura, quedando el puntero del archivo al principio del mismo: 
    si el archivo no existía se crea, y si existía se devolverá false generándose también un error de tipo E_WARNING.
x+: igual que la opción anterior pero abre el archivo en modo lectura y escritura.

c: abre el archivo en modo sólo escritura, quedando el puntero del archivo al principio del mismo: 
    si el archivo no existía se creará. Si existe no se eliminará su contenido (como sucede con w) ni se devolverá 
    un error (como con x).

c+: lo mismo que la opción anterior pero crea y abre el archivo en modo lectura y escritura.

IMPORTANTE: los sistemas Windows usan \r\n como caracteres de final de línea, los basados en UNIX usan \n 
y los sistemas basados en sistemas MACintosh usan \r: para evitar incompatibilidades es recomendable usar 
el modificador b.

Una vez hayamos terminado de realizar las operaciones deseadas con el archivo es necesario cerrarlo usando la 
función fclose(), que escribirá en él los cambios pendientes en el buffer (memoria en la que se guardan los datos 
antes de ir escribiéndolos).

Como puedes ver en el siguiente ejemplo, fopen() nos devuelve un puntero al archivo:

*/

    // Abrir el archivo, creándolo si no existe
     $archivo = fopen("datos.txt", "w+b"); 
        
    if( $archivo == false ) {
        echo "Error al crear el archivo";
    }
    else {
        echo "El archivo ha sido creado";
    }
    
    // Cerrar el archivo  
    fclose($archivo);   






?>