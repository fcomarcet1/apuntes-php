<?php

/*
Los archivos de texto delimitado o csv son archivos de texto normales en los que se utiliza un determinado carácter para separar cada campo, como en el siguiente ejemplo en el que los datos de cada línea se encuentran separados mediante punto y coma:

    Ej
        Pepe;Lotas;Perez;http://www.pepelotas.es
        Paco;Marcet;Prieto;http://www.fcomarcet.es
Para el manejo de archivos de texto delimitado o csv disponemos de las funciones de PHP fgetcsv() y fputcsv(), con las que podremos leer y escribir en ellos respectivamente, si bien podemos usar también las explicadas anteriormente como fwrite(), fread(), etc.

A continuación te mostramos un ejemplo de cómo escribir en un archivo pasando como parámetro a fputcsv() tanto o una cadena de texto como un array, que contienen los datos que se desean escribir:

*/



	$linea  = "Antonia,Martel,Calvo,http://www.antoniamc74924.com/";
	$aDatos = array("Rosa", "Castellano", "Herrera", "http://www.rosach3729023.com/");

	// Abrimos el archivo situando el puntero al final del archivo:
	$archivo = fopen( "datos.csv", "ab" );

	fputcsv( $archivo, split(",", $linea), ";" );
	fputcsv( $archivo, $aDatos, ";" );

	fclose( $archivo );

?>