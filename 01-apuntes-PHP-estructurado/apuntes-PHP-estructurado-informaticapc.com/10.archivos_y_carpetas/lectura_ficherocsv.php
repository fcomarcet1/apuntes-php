<?php 

/*
Para obtener los datos de un archivo csv o delimitado usaremos fgetcsv() pasándole como parámetro un puntero al archivo abierto con fopen() y como segundo parámetro (opcional) el número de caracteres a leer, debiendo ser un número mayor que la longitud de la línea más larga (no se leerán caracteres de otra línea). En PHP 5 este parámetro ya no es necesario (aunque será ligeramente más lento).

Como tercer y cuarto parámetros (opcionales también) podemos indicar el carácter delimitador y el carácter de cierre, respectivamente.

Si fgetcsv() encuentra una línea en blanco se devolverá un array conteniendo un sólo campo con valor null, no siendo considerado como un error.

*/

$archivo = fopen( "datos.csv", "rb" );

// Leer la primera línea:
 $aDatos = fgetcsv( $archivo, 100, ";");
print_r( $aDatos );

echo "<br />";

// Tras la lectura anterior, el puntero ha quedado en la segunda línea:
$aDatos = fgetcsv( $archivo, 100, ";" );
print_r( $aDatos );
echo "<br />-------------------------------------<p />";


// Volvemos a situar el puntero al principio del archivo:
fseek($archivo, 0);

// Recorremos el archivo completo:
 while( feof($archivo) == false ) {
     $aDatos = fgetcsv( $archivo, 100, ";");
     
    echo "Nombre: ".$aDatos[0]."<br />";
    echo "Apellido 1: ".$aDatos[1]."<br />";
    echo "Apellido 2: ".$aDatos[2]."<br />";
    echo "WEB: ".$aDatos[3]."<br />";
    echo "--------------------------<br />";
}

fclose( $archivo );
