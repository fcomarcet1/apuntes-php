<?php

/*
Para insertar texto en un archivo usaremos las funciones fwrite() o fputs(),las cuales devolverán false en 
caso de error.

Con la función de PHP set_file_buffer() es posible definir el tamaño del buffer.

Usando la función fflush() podemos forzar que se escriban en el archivo los cambios pendientes en el buffer de 
escritura. Cuando se llama a fclose() se escribirán también los cambios pendientes.
*/


    // Abrir el archivo, creándolo si no existe:
    $archivo = fopen("datos.txt","w+b");

    if( $archivo == false ) {

      echo "Error al crear el archivo";
    }
    else {
        // Escribir en el archivo:
         fwrite($archivo, "Estamos probando\r\n");
         fwrite($archivo, "el uso de archivos ");
         fwrite($archivo, "en PHP");


        // Fuerza a que se escriban los datos pendientes en el buffer:
         fflush($archivo);
    }

    // Cerrar el archivo:
    fclose($archivo);
?>

<?php 
//También podemos insertar texto en un archivo a partir de una cadena de texto usando la función 
//de PHP file_put_contents():

    // el fichero datos.txt contiene los datos 
    $cadena = file_get_contents("datos.txt");

    $cadena .= "\r\nMe encanta PHP!";
    file_put_contents("datos.txt", $cadena);

?>