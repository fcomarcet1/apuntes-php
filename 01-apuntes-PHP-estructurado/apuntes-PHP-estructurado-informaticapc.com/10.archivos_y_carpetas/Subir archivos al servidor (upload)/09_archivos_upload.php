<?php
/*
Para hacer upload (subir archivos al servidor web en primer lugar debemos crear un formulario HTML con los atributos method="post" y enctype="multipart/form-data", y que contenga un componente de tipo <input type="file" ... />.

Tras seleccionar un archivo y enviarse el formulario, la información del archivo subido quedará guardada en el array asociativo $_FILES, que tiene las siguientes claves:

	name: nombre original del archivo.
	tmp_name: nombre temporal del archivo subido junto con la ruta temporal (los archivos subidos se guardan en un directorio temporal).
	type: tipo de archivo.
	error: código de error, si sucedió alguno.
	size: tamaño del archivo en bytes.
	
Para acceder al archivo subido utilizaremos las funciones de PHP is_uploaded_file() (para comprobar si el archivo fue subido al directorio temporal usando el método POST) y move_uploaded_file() (mueve el archivo temporal hacia la ruta actual).
*/


print_r($_FILES);

	if( empty($_FILES['txtFile']['name']) == false )
	{
		if (is_uploaded_file($_FILES['txtFile']['tmp_name'])) 
		{			
			if( move_uploaded_file($_FILES['txtFile']['tmp_name'], $_FILES['txtFile']['name']) == false )
				echo "No se ha podido el mover el archivo.";
			else
				echo "Archivo [".$_FILES['txtFile']['name']."] subido y movido al directorio actual.";
		} 
		else 
		{
		   echo "Posible ataque al subir el archivo [".$_FILES['txtFile']['nombre_tmp']."]";
		}
	}
	else
	{
		echo "No se seleccion� ning�n archivo.";
	}

?>