<?php
/*
Para copiar y eliminar archivos disponemos de las funciones copy() y unlink(), respectivamente.

Hay que tener en cuenta que al copiar un archivo a otro directorio, si existe uno con el mismo nombre será sobreescrito.

Si lo que necesitamos es renombrar o mover archivos, en ambos casos utilizaremos la función rename().

Todas estas funciones devuelven true o false si ha ocurrido algún error.
*/


	// Copiar el archivo a otra ruta;
	copy("datos.txt", "c:\\datos.txt");

	// Copiar el archivo con otro nombre:
	copy("datos.txt", "datos-2.txt");				
	copy("datos.txt", "datos-3.txt");
	copy("datos.txt", "datos-4.txt");

	// Renombrar el archivo:
	rename("datos-2.txt", "datos---2.txt");

	// Renombrar carpetas:
	rename("miCarpeta1", "miCarpeta1-1");
	rename("./miCarpeta2", "./miCarpeta2-2");

	// Mover el archivo:
	rename("datos-3.txt", "c:\\datos-3-3.txt");

	// Eliminar el archivo:
	unlink("datos-4.txt");

	echo "Proceso finalizado";

?>