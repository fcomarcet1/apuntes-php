<?php
/*
La función de PHP fread() nos devolverá una cadena de texto con el contenido leído desde el archivo que 
le indiquemos como primer parámetro.

Como segundo parámetro opcional, podemos indicar el número de bytes que se deben leer (si necesitamos 
leer todo el archivo podemos usar la función filesize()).

Tras usar fread() el puntero al archivo quedará en la última posición leída: la función: de ser necesario 
usaremos rewind() para volver a situarlo al principio del mismo.

Según la documentación oficial de PHP, en sistemas operativos que diferencian entre archivos de texto y 
archivos binarios (como Windows) se recomienda abrir el fichero usando el modificador b.
*/



// Abrir el archivo en modo de sólo lectura:
$archivo = fopen("datos.txt", "rb");

if ($archivo == false) {
    echo "Error al abrir el archivo";
} else {
    $cadena1 = fread($archivo, 18); // Leemos un determinado número de caracteres
    rewind($archivo);   // Volvemos a situar el puntero al principio del archivo
    $cadena2 = fread($archivo, filesize("datos.txt"));  // Leemos hasta el final del archivo

    if (($cadena1 == false) || ($cadena2 == false)) {

        echo "Error al leer el archivo";
    } else {
        echo "<p>\$contenido1 es: [" . $cadena1 . "]</p>";
        echo "<p>\$contenido2 es: [" . $cadena2 . "]</p>";
    }
}

// Cerrar el archivo:
fclose($archivo);
?>


<?php

/*
file_get_contents(), puesto que con ella podemos obtener todo el contenido del archivo en una cadena de texto, 
pudiendo también indicarle la posición inicial a partir de la que deseamos extraer el texto (comenzando desde cero), 
y el número de caracteres a obtener. Devuelve false en caso de error.
*/

$cadena = file_get_contents("datos.txt");
    echo "<p>\$cadena es [" . $cadena . "]</p>";

$cadena = file_get_contents("datos.txt", null, null, 3, 12);
    echo "<p>Ahora \$cadena es [" . $cadena . "]</p>";



?>


<?php 
/*
Otra función que te puede resultar interesante es file(), con la que obtendremos un array que contendrá cada línea 
de texto del archivo en una posición del mismo. Devuelve false si se ha producido un error.
*/

$aCadena = file("datos.txt");
    print_r($aCadena);

?>