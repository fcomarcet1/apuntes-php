<?php

/*
La función de PHP getcwd() nos devuelve el directorio actual, y con chdir() podremos cambiar a otro.

Para crear un directorio usaremos la función mkdir(), indicando como primer parámetro su nombre y como segundo parámetro (opcional) los permisos que deberá tener (sólo en sistemas UNIX, no es aplicable en Windows).

Asimismo, podremos también crear directorios de forma recursiva asignando el valor true como tercer parámetro.

Si deseamos eliminar un directorio vacío cuyos permisos lo permitan, usaremos la función rmdir() (no los borra de forma recursiva).
*/

	// Obtener el directorio actual:
	echo "El directorio actual es: [".getcwd()."]<br />";

    // Cambiar el directorio actual:
    chdir("c:\\");

	// Obtener el directorio actual:
	echo "Ahora el directorio actual es: [".getcwd()."]<br />";

	// Crear directorios:
	mkdir("miCarpeta58975-01-1");
	mkdir("./miCarpeta58975-02-1");
	mkdir("./miCarpeta58975-03-1/miCarpeta58975-03-2/miCarpeta589752-03-3/", null, true);

	// Renombrar directorio
	rename("miCarpeta58975-01-1", "miCarpeta58975--01--1");

	// Borrar un directorio (no borra los subdirectorios):
	rmdir("./miCarpeta58975-02-1");

?>
