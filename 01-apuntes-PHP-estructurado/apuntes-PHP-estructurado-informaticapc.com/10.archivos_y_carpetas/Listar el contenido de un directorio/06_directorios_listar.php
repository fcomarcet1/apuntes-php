<?php
/*
n caso de necesitar obtener el contenido de un directorio disponemos de la función glob() que nos devolverá el contenido según el patrón especificado.

También nos serán muy útiles opendir() que nos devolverá un recurso con el que podremos gestionar el contenido del directorio y readdir(), que devuelve el nombre del siguiente archivo leído desde el recurso obtenido con opendir().

IMPORTANTE: readdir() devuelve false en caso de error, pero también puede devolver cero o cadena vacía que serían evaluados como tal: al realizar la comparación utiliza los operadores === o !==, que tienen en cuenta también si los valores son del mismo tipo de datos.

*/


	// Cambiar el directorio actual:
	chdir("c:\\");

	// Mostrar el contenido del directorio actual:
	foreach( glob("*.*") as $archivo ) 
		echo $archivo."<br />";

	echo "<p>-------------------------</p>";
 
	 // Otra forma de obtener el contenido del directorio:

	// Obtenemos el contenido del directorio (se usa  doble barra inversa, en Windows):
	$aArchivos = opendir("c:\\windows");

    while( ($archivo = readdir($aArchivos)) !== false ) {
        echo $archivo."<br/>";
    }

	echo "<p>-------------------------</p>";
	echo "<p>Finalizado</p>";

?>
