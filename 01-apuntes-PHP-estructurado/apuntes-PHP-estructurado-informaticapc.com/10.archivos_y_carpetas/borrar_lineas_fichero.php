<?php 
/*
Para borrar líneas en un archivo de texto es necesario leer el archivo, eliminar las líneas deseadas y volverlo a escribir. Una forma de hacerlo es usando las función file(), que carga cada línea de texto en un array, junto con array_splice() que nos permite eliminar posiciones de un array
*/

// Obtener cada línea en un array:
$array_Lineas = file("datos.txt");
print_r($array_Lineas);

echo "<p>CONTENIDO DEL ARCHIVO</p>";
echo "<p>=====================</p>";

// Mostrar el contenido del archivo:
foreach( $array_Lineas as $linea )
    echo $linea."<br/ >";
echo "<p>Borrando la tercera línea...</p>";

// Borrar el tercer elemento del array (la tercera línea):
array_splice($array_Lineas, 2, 1);
print_r($array_Lineas);

// Abrir el archivo:
 $archivo = fopen("datos.txt", "w+b");

// Guardar los cambios en el archivo:
foreach( $array_Lineas as $linea )
     fwrite($archivo, $linea);

echo "<p>CONTENIDO DEL ARCHIVO</p>";
echo "<p>=====================</p>";

// Mostrar el contenido del archivo:
foreach( $array_Lineas as $linea )
    echo $linea."<br/ >";


fclose($archivo);
