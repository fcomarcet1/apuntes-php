<?php

	$archivo_origen1 = "logo.jpg";
	$archivo_origen2 = "archivo.html";
	$archivo_zip = "comprimido.zip";

	// Creamos una instancia de la clase ZipArchive:
	$zip = new ZipArchive();

    // Creamos el archivo zip:
	if ($zip->open($archivo_zip, ZIPARCHIVE::CREATE) === true ) 
	{
		// A�adimos archivos:
		$zip->addFile( $archivo_origen1 );
		$zip->addFile( $archivo_origen2 );

		// Cerramos el archivo zip:
		$zip->close();

		echo "Proceso finalizado";
	}
	else
	{
       echo "Ha ocurrido un error";
    }

?>