<?php

	$archivo_zip = "comprimido.zip";

	// Creamos una instancia de la clase ZipArchive:
	$zip = new ZipArchive();

    // Abrimos el archivo zip:
	if( $zip->open($archivo_zip) === true ) 
	{
	    $zip->extractTo('./temp/');
		$zip->close();

		echo 'Archivo descomprimido';
	}
	else 
	{
		echo 'Error al descomprimir';
	}

?>