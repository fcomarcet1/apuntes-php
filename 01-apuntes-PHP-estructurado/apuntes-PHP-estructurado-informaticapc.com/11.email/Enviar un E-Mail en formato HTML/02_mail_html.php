<?php
	/*
		Observa que hemos añadido el Content-type adecuado para enviar el correo electrónico en formato HTML, y que usamos la etiqueta <br /> para insertar saltos de línea.

		Fíjate también que hacemos uso de la función de PHP nl2br(): esto es así porque en los <textarea> se guardan los saltos de línea con los caracteres utilizados por el sistema operativo, y puesto que vamos a enviar el Email en formato HTML, con dicha función los convertimos a la etiqueta HTML <br> (si no lo hiciéramos, todo el cuerpo del mensaje se mostraría en una sola línea).
			
	*/



	/* 
        En caso de que no puedas enviar los correos electr�nicos y no puedas o quieras
        editar el archivo de configuraci�n 'php.ini', descomenta las siguientes l�neas con
        las que modificamos la configuraci�n en tiempo de ejecuci�n. Si es necesario, modifica 
		el valor adecuado.
	*/
	//ini_set('SMTP', "localhost");
	//ini_set('smtp_port', 25);
	//ini_set('sendmail_from', "postmaster@localhost.com");
	//ini_set('display_errors', "On");    // Mostrar los errores (usar s�lo durante las pruebas)

    // Comprobar si llegaron los datos requeridos:
    if(  !empty($_POST) && 
		 (isset($_POST['txtNombre'])  && !empty($_POST['txtNombre']))  &&
		 (isset($_POST['txtMail'])	  && !empty($_POST['txtMail']))	   &&
		 (isset($_POST['txtMensaje']) && !empty($_POST['txtMensaje'])) 
	  )
	{

		$mensaje  = "Mensaje de: ".$_POST['txtNombre']."<br />";
		$mensaje .= "EMail: ".$_POST['txtMail']."<p />";
		$mensaje .= nl2br($_POST['txtMensaje']);
		
		// Indicar cabecera con el nombre del remitente. Si no indicamos la direcci�n de correo puede que 
		// no se realice el env�o a a otros servicios como Hotmail o Yahoo
		$headers  ="Content-type: text/html; charset=iso-8859-1;";			 // Especifica correo en formato HTML
		$headers .= "From: TU_NOMBRE <TU_CUENTA_DE_EMAIL@TU_SERVIDOR.com>";

		// IMPORTANTE: debes sustituir la direcci�n de correo por aquella en que deseas recibir el EMail:
		$ok = mail( trim($_POST['txtMail']), "Mensaje de prueba", $mensaje, $headers );

		if( $ok == true )
			echo "<p>El E-Mail ha sido enviado</p>";
		else
			echo "<p>ERROR al enviar el E-Mail</p>";

		echo "<p>Haz <a href='02_mail_html.html'>click para volver al formulario</a></p>";

	}
	else
	{

		$html  = "<html>";
		$html .= "<head>";

		// Despu�s de cuatro segundos de mostrarse esta p�gina web de error se redirigir�a a la URL especificada.
		$html .= "<meta http-equiv='refresh' content='4;url=02_mail_html.html'>";

		$html .= "</head>";
		$html .= "<body>";
		$html .= "No han llegado todos los datos. En unos segundos ser� redirigido a la p�gina principal.";
		$html .= "</body>";
		$html .= "</html>";

		echo $html;

	}

?>

