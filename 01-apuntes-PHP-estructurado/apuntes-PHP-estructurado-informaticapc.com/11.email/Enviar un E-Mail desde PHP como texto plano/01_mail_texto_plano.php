<?php

	/* 
        En caso de que no puedas enviar los correos electr�nicos y no puedas o quieras
        editar el archivo de configuraci�n 'php.ini', descomenta las siguientes l�neas con
        las que modificamos la configuraci�n en tiempo de ejecuci�n. Si es necesario, modifica
		el valor adecuado.
	*/
	//ini_set('SMTP', "localhost");
	//ini_set('smtp_port', 25);
	//ini_set('sendmail_from', "postmaster@localhost.com");
	//ini_set('display_errors', "On");    // Mostrar los errores (usar s�lo durante las pruebas)

    // Comprobar si llegaron los datos requeridos:
    if(  !empty($_POST) && 
		 (isset($_POST['txtNombre'])  && !empty($_POST['txtNombre']))  &&
		 (isset($_POST['txtMail'])	  && !empty($_POST['txtMail']))	   &&
		 (isset($_POST['txtMensaje']) && !empty($_POST['txtMensaje'])) 
	  )
	{

		$mensaje = "Mensaje de: ".$_POST['txtNombre'].PHP_EOL;
		$mensaje .= "EMail: ".$_POST['txtMail'].PHP_EOL.PHP_EOL;
		$mensaje .= $_POST['txtMensaje'];
		
		// Indicar cabecera con el nombre del remitente. Si no indicamos la direcci�n de correo puede que 
		// no se realice el env�o a a otros servicios como Hotmail o Yahoo
		$cabecera = "From: TU_NOMBRE <TU_CUENTA_DE_EMAIL@TU_SERVIDOR.com>";

		// IMPORTANTE: debes sustituir la direcci�n de correo por aquella en que deseas recibir el EMail:
		$ok = mail( trim($_POST['txtMail']), "Mensaje de prueba", $mensaje, $cabecera );

		if( $ok == true )
			echo "<p>El E-Mail ha sido enviado</p>";
		else
			echo "<p>ERROR al enviar el E-Mail</p>";

		echo "<p>Haz <a href='01_mail_texto_plano.html'>click para volver al formulario</a></p>";

	}
	else
	{

		$html  = "<html>";
		$html .= "<head>";

		// Despu�s de cuatro segundos de mostrarse esta p�gina web de error se redirigir�a a la URL especificada.
		$html .= "<meta http-equiv='refresh' content='4;url=01_mail_texto_plano.html'>";

		$html .= "</head>";
		$html .= "<body>";
		$html .= "No han llegado todos los datos. En unos segundos ser� redirigido a la p�gina principal.";
		$html .= "</body>";
		$html .= "</html>";

		echo $html;

	}

?>

