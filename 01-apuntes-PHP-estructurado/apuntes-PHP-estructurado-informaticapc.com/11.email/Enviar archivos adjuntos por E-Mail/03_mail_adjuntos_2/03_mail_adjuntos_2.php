<?php

	/* 
        En caso de que no puedas enviar los correos electr�nicos y no puedas o quieras
        editar el archivo de configuraci�n 'php.ini', descomenta las siguientes l�neas con
        las que modificamos la configuraci�n en tiempo de ejecuci�n. Si es necesario, modifica
		el valor adecuado.
	*/
	//ini_set('SMTP', "localhost");
	//ini_set('smtp_port', 25);
	//ini_set('sendmail_from', "postmaster@localhost.com");
	//ini_set('display_errors', "On");    // Mostrar los errores (usar s�lo durante las pruebas)

    // Comprobar si llegaron los datos requeridos:
    if(  !empty($_POST) && 
		 (isset($_POST['txtNombre'])  && !empty($_POST['txtNombre']))  &&
		 (isset($_POST['txtMail'])	  && !empty($_POST['txtMail']))	
	  )
	{

		// Indicar cabecera con el nombre del remitente. Si no indicamos la direcci�n de correo puede que 
		// no se realice el env�o a a otros servicios como Hotmail o Yahoo
		$cabecera = "From: TU_NOMBRE <TU_CUENTA_DE_EMAIL@TU_SERVIDOR.com>\r\n";

		$datos   = "";
		$mensaje = "";
		$contenido_archivos = "";
		$hay_archivos = false;


		// Creamos una cadena aleatoria como separador entre cuerpo y archivos adjuntos:
		$separador = md5(uniqid(time()));

		// Recorremos el array para leer cada archivo recibido:
		foreach( $_FILES as $adjunto )
		{

			// Comprobamos si el archivo fue subido, y leemos su contenido:
		    if( is_uploaded_file($adjunto['tmp_name']) )
			{
				 $hay_archivos = true;

 				 // Leemos el archivo obteni�ndolo como una cadena de texto:
				 $archivo = fopen($adjunto['tmp_name'], "rb");
				 $datos = fread( $archivo, filesize($adjunto['tmp_name']) );
				 fclose($archivo);

				 // Dividimos la cadena de texto en varias partes m�s peque�as:
				 $datos = chunk_split( base64_encode($datos) );			

				// Continuamos construyendo el cuerpo del mensaje, a�adiendo el archivo:
				$contenido_archivos .= "--".$separador."\r\n";
				$contenido_archivos .=	"Content-Type: ".$adjunto['type']."; name='".$adjunto['name']."'\r\n";
				$contenido_archivos .= "Content-Transfer-Encoding: base64\r\n";
				$contenido_archivos .= "Content-Disposition: attachment; filename='".$adjunto['name']."'\r\n\r\n";
				$contenido_archivos .= $datos."\r\n\r\n";
			}

		}


		// Si se subieron archivos creamos las cabeceras necesarias:
		if( $hay_archivos == true )
		{
			// Creamos la cabecera del mensaje:
			$cabecera .= "MIME-Version: 1.0\r\n".
						 "Content-Type: multipart/mixed; boundary=\"".$separador."\"\r\n\r\n";

			// Construimos el cuerpo del mensaje para el texto, a�adiendo al final los archivos adjuntos:
			$mensaje  = "--".$separador."\r\n";
			$mensaje .= "Content-Type:text/plain; charset='iso-8859-1'\r\n";
			$mensaje .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
			$mensaje .= $_POST['txtMensaje']."\r\n\r\n".$contenido_archivos;

			// Separador de final del mensaje:
			$mensaje .= "--".$separador."--";
		}
		else
		{
			// No se adjunt� ning�n archivo, enviamos s�lo el texto del mensaje:

			$mensaje  = "Mensaje de: ".$_POST['txtNombre'].PHP_EOL;
			$mensaje .= "EMail: ".$_POST['txtMail'].PHP_EOL.PHP_EOL;
			$mensaje .= $_POST['txtMensaje'];
		}


		// IMPORTANTE: debes sustituir la direcci�n de correo por aquella en que deseas recibir el EMail:
		$ok = mail( trim($_POST['txtMail']), "Mensaje de prueba", $mensaje, $cabecera );

		if( $ok == true )
			echo "<p>El E-Mail ha sido enviado</p>";
		else
			echo "<p>ERROR al enviar el E-Mail</p>";

		echo "<p>Haz <a href='03_mail_adjuntos_2.html'>click para volver al formulario</a></p>";

	}
	else
	{

		$html  = "<html>";
		$html .= "<head>";

		// Despu�s de cuatro segundos de mostrarse esta p�gina web de error se redirigir�a a la URL especificada.
		$html .= "<meta http-equiv='refresh' content='4;url=03_mail_adjuntos_2.html'>";

		$html .= "</head>";
		$html .= "<body>";
		$html .= "No han llegado todos los datos. En unos segundos ser� redirigido a la p�gina principal.";
		$html .= "</body>";
		$html .= "</html>";

		echo $html;

	}

?>