<?php

    /*
        IMPORTANTE: descarga PHPMailer para tu versi�n de PHP y copia los archivos
        inclu�dos con 'require()' en la misma carpeta del ejemplo.

        http://sourceforge.net/projects/phpmailer/files/phpmailer%20for%20php5_6/
    */
	require("class.phpmailer.php");
	require("class.smtp.php");

    // Comprobar si se llegaron los datos
	if( !empty($_POST) &&
		(isset($_POST['txtNombre'])  && !empty($_POST['txtNombre'])) &&
		(isset($_POST['txtMail'])    && !empty($_POST['txtMail']))	 &&
		(isset($_POST['txtMensaje']) && !empty($_POST['txtMensaje']))
	   )
	{

		try
		{

			$mail = new PHPMailer();			// Crear una instancia de la Clase

			// Configuraciones para GMail
			$mail->Host = "smtp.gmail.com";		// DATOS SERVIDOR CORREO
			$mail->SMTPAuth = true;				// Indicamos que el servidor requiere autenticaci�n
			$mail->SMTPSecure = "tls";
			$mail->Port = "587";
			
			// Datos de acceso a tu cuenta en GMail (usuario y contrase�a):
			$mail->Username = "TU_EMAIL@TU_SERVIDOR.com";
			$mail->Password = "TU_PASSWORD";

			// FROM (la direcci�n de correo y el nombre que queremos que vea el usuario que lee nuestro correo):
			$mail->From = "test@prueba.com";
			$mail->FromName = "TU NOMBRE";
			
			// Datos del Mensaje:
			$mail->AddAddress($_POST['txtMail'], "DESTINATARIO");	// Email del DESTINATARIO
			$mail->Subject = "Asunto del mensaje";					// ASUNTO
			$mail->IsHTML(false);									// Indica si el CUERPO estar� en formato HTML
			
			// Cuerpo del mensaje:
			$mail->AltBody = $_POST['txtMensaje'];		 // ... si el proveedor de correo de destino no soporta HTML
			$mail->Body = nl2br($_POST['txtMensaje']);	 // ... en HTML (obligatorio)

			// Si se adjunt� un archivo se a�ade al EMail:
			if( empty($_FILES['txtFile']['name']) == false )
				$mail->AddAttachment($_FILES['txtFile']['tmp_name'], $_FILES['txtFile']['name']);

			// Enviar el EMail
			if(!$mail->Send())	
				echo "<p>No se puedo enviar el EMail</p>";
			else
				echo "<p>EMail enviado correctamente</p>";

		}
		catch(Exception $e )
		{
			echo "<p>Ha ocurrido un error al enviar el EMail</p>";
		}
         
	}
	else
	{
		echo "<p>No llegaron datos</p>";
	}

	echo "<p>Haz <a href='04_mail_phpmailer.html'>click para volver al formulario</a></p>";

?>

