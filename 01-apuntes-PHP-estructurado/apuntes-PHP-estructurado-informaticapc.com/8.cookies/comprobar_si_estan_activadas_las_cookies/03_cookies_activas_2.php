<?php

/*
hemos creado la función comprobarCookies() con la que podremos comprobar si las Cookies están activas 
en el navegador web del usuario.

Tal y como hemos comentado en el apartado anterior, será necesario recargar la página web o cargar otra 
para que se actualice $_COOKIE (y poder entonces usar nuestra función), por lo tanto en el ejemplo hemos 
usado la función de PHP header() para cargar otra página.

*/

    function comprobarCookies()
    {
        $activa = false;

        if( isset($_COOKIE['nombre']) )
            $activa = true;

        return $activa;
	}

	// ---------------

    if( comprobarCookies() == true ){
        echo("Las Cookies est�n activas");
    }  
    else{
        echo "Las Cookies est�n desactivadas";
    }
		

?>