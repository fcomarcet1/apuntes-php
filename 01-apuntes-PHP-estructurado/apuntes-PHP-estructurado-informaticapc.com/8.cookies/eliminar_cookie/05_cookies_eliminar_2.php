<?php
//Para eliminar una Cookie en PHP tan sólo debemos volver a crearla indicando una fecha anterior a la actual.

     if( isset($_COOKIE['nombre']) )
    {
        echo "Eliminamos la Cookie";
         setcookie("nombre", "", time() - 1 );
    }
    else
    {
        echo "No existe la Cookie";
    }
    echo "<p><a href='05_cookies_eliminar_2.php'>Haz clic para recargar la página y comprobar si existe la Cookie</a></p>";
?>