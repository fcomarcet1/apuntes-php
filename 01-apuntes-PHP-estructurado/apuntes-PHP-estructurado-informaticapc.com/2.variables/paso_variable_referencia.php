<?php
/*
Cuando pasamos como parámetro una variable a una función en PHP lo que hacemos realmente es 
pasar su valor (una copia de la misma), por lo que si ésta es modificada dentro de la función 
el cambio no permanecerá fuera de ella.

Se pueden pasar por referencia:

    ->Variables, tal como miFuncion( $a )
    ->Nuevas declaraciones, tal como miFuncion( new objeto() )
    ->Referencias devueltas desde funciones
*/


function funcion1($n1) {
    $n1 = $n1 + 10;
}
// ---------------------


 // Pasar por referencia
function funcion2(&$n1) {
    $n1 = $n1 + 10;
}
// ---------------------




class Clase1
{
    private $valor = null;
    public function clase1()
    {
        $this->valor = 100;
    }

    // Devolver por referencia
    public function &getValor()    
    {
        return $this->valor;
    }
}
// ---------------------


$num1 = 5;
$num2 = 10;
$num2 = &$num1;    // Asignación por referencia: '$num2' no apunta a '$num1', ambas apuntan al mismo lugar

   
echo "\$num1 = [" . $num1 . "] y \$num2 = [" . $num2 . "]<br />";  // Devuelve: "$num1 = [5] y $num2 = [5]"
  
$num2 = 20;

echo "\$num1 = [" . $num1 . "] y \$num2 = [" . $num2 . "]<br />";    // Devuelve: "$num1 = [20] y $num2 = [20]"

$n1 = 2;


funcion1($n1);
echo "Ahora \$n1 = [" . $n1 . "]<br />";    // Devuelve: "Ahora $n1 = [2]"

funcion2($n1);
echo "Ahora \$n1 = [" . $n1 . "]<br />";    // Devuelve: "Ahora $n1 = [12]"

$obj = new Clase1();
$x = &$obj->getValor();

echo "\$x = [" . $x . "]<br />";            // Devuelve: "$x = [100]"

$x = 200;
$x = &$obj->getValor();

// Devuelve: "$x = [200]" (¡¡ ojo porque la propiedad es 'private' y no debiera ser modificada fuera de la clase !!)
echo "\$x = [" . $x . "]";


?>