<?php

// comprobamos si el nombre 'PEPE' existe en el array '$array_nombres', 
// quedando la variable '$posicion' con un valor de '-1' si no se ha encontrado, 
// o conteniendo la posición en la que se ha encontrado


$array_nombres = array("MARIA", "JUAN", "PEDRO", "ISABEL", "PEPE", "FERNANDO", "ROBERTO");
$contador;
$posicion = -1;

for ($contador = 0; $contador < count($array_nombres); $contador++) {

    if ($array_nombres[$contador] == "PEPE") {
        $posicion = $contador;

        // break forzamos el que se salga del bucle si se encuentra el dato
        break;
    }
}
if ($posicion == -1) {

    echo "No se ha encontrado el nombre";
} else {

    echo "PEPE está en la posición [" . $contador . "]";
}
?>       


<?php
$aColores1 = array("color1" => "rojo", "color2" => "verde", "color3" => "azul", "color4" => "verde");
$aColores2 = array(37, "Pedro", "color1" => "rojo", "color2" => "verde", "color3" => "azul");

if (in_array("rojo", $aColores1) == true)
    echo "Se ha encontrado el valor 'rojo' en \$aColores1<br/>";
if (in_array("magenta", $aColores1) == false)
    echo "NO se ha encontrado el valor 'magenta' en \$aColores1<br/>";
if (in_array(37, $aColores2) == true)
    echo "Se ha encontrado el número 37 en \$aColores2<br/>";
if (in_array(85, $aColores2) == false)
    echo "NO se ha encontrado el número 85 en \$aColores2<br/>";
?>