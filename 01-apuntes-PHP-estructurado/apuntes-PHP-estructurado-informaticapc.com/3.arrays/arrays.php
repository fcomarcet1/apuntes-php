<?php
    // Array de 7 elementos con el nombre de un día de la semana en cada uno de ellos:
    $array_Dias = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");

    // Array de 4 elementos con un número contenido en cada posición:
    $array_Numeros = array(33, 12, 83, 55);

    // Array de 5 elementos, sin asignar valor en ninguna posición:
    $array_Nombres = array(5);

    // Array vacío, sin posiciones definidas:
    $array_Datos = array();

    // Mostrar datos de un array:
    echo "El primer día es el: ".$aDias[0]."<br/>";
    echo "El segundo día es el: ".$aDias[1]."<br/>";
    echo "El tercer día es el: ".$aDias[2]."<br/>";

    echo "El segundo número es el: ".$aNumeros[1]."<br/>";


?>