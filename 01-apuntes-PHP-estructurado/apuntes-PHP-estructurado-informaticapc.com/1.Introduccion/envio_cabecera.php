<?php

/*Aparte de para redirigir a otra página también usaremos header() para enviar encabezados al navegador web. 
Para obtener más información sobre encabezados HTTP consulta el enlace facilitado al final de este apartado.

En el siguiente ejemplo enviamos encabezados que informan de que será enviado un archivo PDF*/
?>

<?php
$archivo = "test.pdf";

    if( file_exists($archivo) )
    {
        // Cabeceras que indican que enviaremos el PDF y su tamaño:
         header("Content-type: application/pdf");
         header("Content-Disposition: attachment; filename=".$archivo);
         header("Content-length: ".filesize($archivo));


        // Leer y enviar el archivo al navegador web:
        readfile($archivo);
    }
?>

<?php
/*
Usaremos las función headers_list() para comprobar si se han definido cabeceras y headers_sent() para 
verificar que efectivamente han sido enviadas (es posible que no lo sean tras procesarse header() sino 
que hayan quedado temporalmente en el buffer de salida).
*/
?>

<?php
    // Enviamos una cabecera:
     header("Status: 404 Not Found");

    // Forzamos que se vacíe el buffer, enviando la cabecera:
     //flush();

    // Mostramos los datos de la cabecera:
     print_r( headers_list() );

     // Comprobamos si se han enviado las cabeceras
     if( headers_sent() == true ) {  
        echo "<p>Se han enviado cabeceras</p>";
    } 
    else {
        echo "<p>No se han enviado cabeceras</p>";
    }    
?>