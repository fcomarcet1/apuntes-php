<?php

/*usaremos uno de los siguientes arrays asociativos:

$_GET: si el formulario contiene el atributo HTML method="get".
$_POST: si el formulario contiene el atributo HTML method="post" (recomendado por cuestiones de seguridad).
$_FILE: si el formulario contiene el atributo HTML enctype="multipart/form-data"  y una etiqueta HTML <input type="file" ... /> 
(usados cuando necesitamos subir archivos (hacer upload para guardarlos en el servidor web o enviarlos por EMail). 

*/
?>

<?php

// Comprobar si el formulario fue enviado (si el array asociativo '$_POST' no está vacío)
if (empty($_POST) == true) {

    // El formulario no ha sido enviado
} 
else {
    echo "<p>Contenido del array asociativo con la información del formulario:</p>";
    print_r($_POST);
    echo "<p/>";
    echo "Fecha: [" . $_POST['txtFecha'] . "]<br/>";
    echo "Numero: [" . $_POST['txtNumero'] . "]<br/>";
    echo "<hr/>";
}
?>

<html>
    <head>
        <title>Curso de PHP - ejemplo de formulario |</title>
    </head>
    <body>
        <form name="frmPrueba" method="post" action="form1.php" >
            <input type="text" name="txtFecha" id="txt1" />
            <input type="text" name="txtNumero" id="txt2" value="texto predefinido" />
            <input type="submit" name="btnEnviar" value="Enviar Formulario" />
        </form>
    </body>
</html>