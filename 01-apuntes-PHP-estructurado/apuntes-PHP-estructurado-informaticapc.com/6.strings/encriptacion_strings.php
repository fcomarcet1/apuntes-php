<?php 

/*
Para encriptar una cadena de texto no se recomienda utilizar las funciones de PHP md5() ni sha1() 
sino en su lugar hash() o crypt(), siendo esta última la más recomendada.

A la función crypt() se le indica como primer parámetro la cadena a encriptar y como segundo parámetro 
opcional una cadena de texto denominada salt en base a la que se realizará la encriptación, dependiendo 
de su formato y longitud el algoritmo de encriptación que será utilizado (algunos de ellos no están incluídos 
en PHP y deben estar disponibles en el sistema operativo),

    crypt(string,salt)

pudiendo ser:

    CRYPT_STD_DES: basado en DES, con un salt de dos caracteres.
    CRYPT_EXT_DES: basado en DES extendido, con un salt de 9 caracteres.
    CRYPT_MD5: Hash MD5 con un salt de doce caracteres.
    CRYPT_BLOWFISH: hash Blowfish con un salt como sigue: '$2a$', un parámetro de costo de dos dígitos, '$' y 22 dígitos del alfabeto (letras mayúsculas/mínúsculas y dígitos).
    CRYPT_SHA256: hash SHA-256 con un salt de dieciséis caracteres prefijado con '$5$'.
    CRYPT_SHA512: Hash SHA-512 con un salt de dieciséis caracteres prefijado con '$6$'.

*/



  if( CRYPT_STD_DES == 1 )
        echo "STANDARD DES (2 caracteres): ".crypt("rasmuslerdorf", "rl")."<p />";
    else
        echo "STANDARD DES no soportado<p />";
    if( CRYPT_EXT_DES == 1 )
        echo "Extended DES (9 caracteres): ".crypt("rasmuslerdorf", "_J9..rasm")."<p />";
    else
        echo "EXTENDED DES no soportado<p />";
    if ( CRYPT_MD5 == 1 )
        echo "MD5 (12 caracteres): ".crypt("rasmuslerdorf", '$1$rasmusle$')."<p />";
    else
        echo "MD5 no soportado<p />";
    if( CRYPT_BLOWFISH == 1 )
        echo "BLOWFISH: ".crypt("rasmuslerdorf", '$2a$07$usesomesillystringforsalt$')."<p />";
    else
        echo "BLOWFISH no soportado<p />";
    if( CRYPT_SHA256 == 1 )
        echo "SHA-256 (16 caracteres): ".crypt("rasmuslerdorf", '$5$rounds=5000$usesomesillystringforsalt$')."<p />";
    else
        echo "SHA-256 no soportado<p />";
    if( CRYPT_SHA512 == 1 )
        echo "SHA-512 (16 caracteres): ".crypt("rasmuslerdorf", '$6$rounds=5000$usesomesillystringforsalt$')."<p />";
    else
        echo "SHA-512 no soportado<p />";
    echo "Máxima longitud de la cadena 'salt' soportada en el sistema: [".CRYPT_SALT_LENGTH."]";
