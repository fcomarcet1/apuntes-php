<?php

/*
Para quitar los espacios en blanco al principio y final de una cadena disponemos de las siguientes funciones de PHP:

trim(): elimina los espacios tanto a principio como a final de una cadena de texto.
ltrim(): quita los espacios al principio de la cadena.
rtrim(): quita los espacios al final de la cadena.

 */
    $cadena1 = "   Curso de PHP  -    ";
    $cadena2 = "       Curso de PHP";
    $cadena3 = "Curso de PHP       ";

    echo "<pre>";
        echo "\$cadena1 : [".trim($cadena1)."]<br />";     // Devuelve: "$cadena1 : [Curso de PHP  -]"
        echo "\$cadena2 : [".ltrim($cadena2)."]<br />";    // Devuelve: "$cadena2 : [Curso de PHP]"
        echo "\$cadena3 : [".rtrim($cadena3)."]<br />";    // Devuelve: "$cadena3 : [Curso de PHP]"
    echo "</pre>";


?>