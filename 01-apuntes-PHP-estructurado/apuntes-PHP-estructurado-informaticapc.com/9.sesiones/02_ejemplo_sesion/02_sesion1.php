<?php

	// Si se usa debe contener (solo caracteres alfanumericos) e ir antes de session_start():
	session_id("idsesion");


	// Iniciar la sesion
	session_start();

	// Variables de sesion:
	$_SESSION['sesion_iniciada'] = true;	
	$_SESSION['nombre'] = "PEDRO";
	$_SESSION['edad'] = 33;

	// Variable de sesion en array:
	$_SESSION['aDatos'] = array();
	$_SESSION['aDatos']['nombre'] = "MARTA";
	$_SESSION['aDatos']['edad'] = 37;

	echo "Identificador de la sesi�n: [".session_id()."]<br/>";

	echo "PAGINA PRINCIPAL<br />";
	echo "================<p />";

	// Mostrar informacion de la sesion:
	echo "Identificador de la sesion: [".session_id()."]<br/>";
	echo "Nombre de la sesion: [".session_name()."]<p/>";

	// Mostrar valores de las variables de sesion creadas:
	echo "Nombre: ".$_SESSION['nombre']."<br />";
	echo "Edad: ".$_SESSION['edad']."<p />";

	echo"var_dump";
	var_dump($_SESSION);

	// Mostrar valores en el array de sesion creado:
	echo "Nombre (en array): ".$_SESSION['aDatos']['nombre']."<br />";
	echo "Edad (en array): ".$_SESSION['aDatos']['edad']."<p />";

	echo "<a href='02_sesion2.php'>Comprobar los valores en otra pagina</a><br/>";
	echo "<a href='02_sesion3.php'>Finalizar la sesion</a>";

?>