<?php 

session_unset();    // Borrar las variables de sesión

/*
Observa que como primer parámetro de la función setcookie() 
(usada para crear una cookie) indicamos el nombre de la sesión, 
y como cuarto parámetro usamos ini_get() para obtener el directorio 
en el que se almacenan las cookies (dicha información se encuentra 
en el archivo de configuración de PHP php.ini).

*/

setcookie(session_name(), 0, 1 , ini_get("session.cookie_path"));    // Eliminar la cookie
session_destroy();  // Destruir la sesión




?>