SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


USE blog_master;


CREATE TABLE usuarios
(
    idusuario       int(255) auto_increment not null,
    nombre          varchar(100) not null,
    apellidos       varchar(100) not null,
    email           varchar(255) not null,
    password        varchar(255) not null,
    fecha_usuario   datetime not null DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT pk_usuarios PRIMARY KEY(idusuario),
    CONSTRAINT uq_email UNIQUE(email)
)
ENGINE=InnoDb 
DEFAULT CHARSET=utf8mb4 ;

CREATE TABLE categorias
(
    idcategoria  int(255) auto_increment not null,
    nombre        varchar(100),
    
    CONSTRAINT pk_categorias PRIMARY KEY(idcategoria)
)
ENGINE=InnoDb DEFAULT 
CHARSET=utf8mb4 ;

CREATE TABLE entradas
(
    identrada     int(255) auto_increment not null,
    id_usuario     int(255) not null,
    id_categoria   int(255) not null,
    titulo        varchar(255) not null,
    descripcion   MEDIUMTEXT,
    fecha_entrada         datetime not null DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT pk_entradas PRIMARY KEY(identrada),
    CONSTRAINT fk_entrada_usuario FOREIGN KEY(id_usuario) REFERENCES usuarios(idusuario) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_entrada_categoria FOREIGN KEY(id_categoria) REFERENCES categorias(idcategoria)  ON DELETE CASCADE ON UPDATE CASCADE


)
ENGINE=InnoDb DEFAULT CHARSET=utf8mb4
;