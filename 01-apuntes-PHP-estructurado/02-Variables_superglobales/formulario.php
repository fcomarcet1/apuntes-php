
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
    </head>
    <body>
        <h2>Formulario GET</h2>
        <form action="recibir_datos_get.php" method="GET">
            <p>
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" value="" required />
            </p>
            <p>
                <label for="apellidos">Apellidos</label>
                <input type="text" name="apellidos" value="" required />
            </p>
            <p>
                <input type="submit" value ="Enviar datos"/>
            </p>
        </form>
        </br>
        <h2>Formulario POST</h2>
        <form action="recibir_datos_post.php" method="POST">
            <p>
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" value="" required />
            </p>
            <p>
                <label for="apellidos">Apellidos</label>
                <input type="text" name="apellidos" value="" required />
            </p>
            <p>
                <input type="submit" value ="Enviar datos"/>
            </p>
        </form>


    </body>
</html>
