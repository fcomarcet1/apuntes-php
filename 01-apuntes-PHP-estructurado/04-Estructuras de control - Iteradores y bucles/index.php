<?php

if (isset($_GET['numero'])) {
    
    $numero = (int)$_GET['numero']; //formateamos a tipo int el valor que llega por GET(string)->Casting o casteo de datos
    var_dump($numero);
}
else{
    
    $numero = 1;
}

echo "<h1>tabla de multiplicar del numero: $numero</h1>";

$contador=0;
//$numero = 7;

while ($contador <= 10) {
    
    $resultado = $numero*$contador;
    echo "$numero x $contador = $resultado </br>";
    $contador++;
    
}

echo '<hr/>';
/* DO-WHILE
 *
do{
    //instruciones
 
 }while(condicion);
 
 */

//si nuestra edad es 18 o + tenemos acceso a todos los locales sino solo al 1

$edad = 18;
$cont = 1;

do{
    
    echo "Tienes acceso al local privado $cont </br>";
    $cont++;
    
}while($edad >= 18 && $cont <=10);