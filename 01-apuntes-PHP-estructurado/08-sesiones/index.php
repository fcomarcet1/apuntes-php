<?php

/* 
 SESSION
 Almacena de manera persistente datos del usuario mientras
 navega hasta que cierra sesion o cierra el navegador.
*/

//iniciar la sesion
session_start();

//variable local
$variable_normal="soy una string<br/>";

//variable de sesion
$texto="Hola soy una sesion activa<br/>";
$_SESSION['variable_persistente'] =$texto;

echo"$variable_normal";
echo "$texto";

