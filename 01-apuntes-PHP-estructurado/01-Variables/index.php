<?php
//Variables
$mi_primera_variable='Bienvenido al curso de PHP';
$numero = 95;

$num = 99;
$decimal = 12.67;
$texto ='esto es un string';
$verdadero = true;
$valor_nulo = null;
$nombres = array('Fran','Pepe');
$array_nombre[] = 'Pepe Lotas';


echo '<h1>'.$mi_primera_variable.'</h1>';
echo $numero.'</br>';


//Constantes
define('nombre', 'fco Marcet Prieto');
define('PI', 3.1416);

echo nombre.'</br>';
echo 'Esto es una constante'.nombre.'</br>';
echo PI;


//Constantes predefinidas
echo PHP_VERSION.'</br>';
echo PHP_OS_FAMILY;



//Debugear variables
$mi_nombre = 'Fco Marcet Prieto';

var_dump($mi_nombre);
var_dump($nombres);
var_dump($array_nombre);


//Imprimir $
echo 'soy un $ '.''.'y esto es una variable y no se muestra $ '.''.$texto.'</br>';

//imprimir comillas dobles "
echo "imprimiento comillas dobles \" ";

?>

