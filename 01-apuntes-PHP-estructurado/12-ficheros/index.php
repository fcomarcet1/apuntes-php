<?php
/*
//abrir archivo
$fichero = 'fichero_texto.txt';
$archivo = fopen("$fichero, "a+");

//leer fichero
//  si solo usamos fgets si tenemos mas de una linea no funciona necesitamos recorrer el fichero mediante un while

while (!feof($archivo)) {
    
    $contenido = fgets($archivo);
    echo "$contenido"."<br/>";
}

//escribir en ficheros
fwrite($archivo, "Soy el nuevo texto introducidodesde php");


//cerrar fichero
fclose($archivo);
?>

*/

/*
//copiar fichero
$fichero = 'fichero_texto.txt';
$nuevo_fichero = 'fichero_texto.txt.bak';
copy($fichero, $nuevo_fichero) or die("Error al copiar");
*/

/*
//renombrar fichero
$fichero_antiguo = 'fichero_texto.txt.bak';
$nuevo_fichero = 'fichero_texto.txt.copy';
rename($fichero_antiguo, $nuevo_fichero);
 */


//eliminar fichero
$nuevo_fichero = 'fichero_texto.txt.copy';
unlink($nuevo_fichero) or die("error al eliminar");


//comprobar existencia del archivo
$fichero = 'fichero_texto.txt';
if(is_file($fichero)){
    echo "El fichero existe";
}
else{
    echo "el fichero no existe";
}