<?php
/*
//VARIABLES LOCALES
Se definen dentro de una funcion y no pueden ser utilizadas fuera de la funcion solo estan
disponibles dentro a no ser que utilicemos un return


//VARIABLES GLOBALES
Se declaran fuera de las funciones y estan disponibles dentro y fuera de la funcion

*/

$frase='Ni los genios son tan genios , ni los mediocres son tan mediocres'; //global
echo $frase;


function mostrarFrase(){
    
    global $frase;
    echo "<h1>$frase</h1>";
    
    $year=2020;
    echo "<h1>$year</h1>";
    return $year;
    
   
}
mostrarFrase();
//al haber usado el return ya podemos usar la variable $year fuera de la funcion
echo "$year";




//FUNCIONES VARIABLES

function buenosDias() {
   return "Hola Buenos dias";  
}


function buenasTardes() {
   return "Hola Buenos Tardes";   
}


function buenasNoches() {
   return "Hola Buenas Noches";  
}


$horario = "buenasNoches";
echo $horario();