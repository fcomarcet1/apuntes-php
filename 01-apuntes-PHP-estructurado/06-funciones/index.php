<?php

/* 
 FUNCIONES
 
 function NombreFuncion($param){
 
        //instrucciones
 } 
 
 */

function mostrarNombre() {
    
    echo "Pepe Lotas</br>";
    echo "Hideo Miyazaki</br>";
    echo "Mario Bros</br>";
    
}

//llamada a la funcion
mostrarNombre();

#################################################
#################################################

function tablaMultiplicar($numero) {
    
    echo "Tabla de Multiplicar del nº:$numero";
    
    for($i=1; $i<=10; $i++ ){
        
        $resultado =$numero * $i;
        echo"$numero x $i = $resultado";
        
        
    }
    
}

if(isset($_GET['numero'])){
    
    //llamada a la funcion
    tablaMultiplicar($_GET['numero']);
    tablaMultiplicar(5);
    tablaMultiplicar(10);
    
}
else {
    echo 'no hay nº';
}

#################################################
#################################################

function calculadora($num1, $num2) {
    
    $suma = $num1+$num2;
    $resta = $num1-$num2;
    $multiplicacion = $num1*$num2;
    $division = $num1/$num2;
    
    echo "Suma = $suma</br>";
    echo "Suma = $resta</br>";
    echo "Multiplicacion =$multiplicacion </br>";
    echo "Division =$division </br>";
    echo "<hr/>";
    
}

//llamada a la funcion
calculadora(10, 3);


//FUNCIONES CON PARAMETROS OPCIONALES


function calculadora2($num1, $num2, $negrita= false) {
    
    $suma = $num1+$num2;
    $resta = $num1-$num2;
    $multiplicacion = $num1*$num2;
    $division = $num1/$num2;
    
    
    if($negrita = true){
        echo '<h1>';
    }
        
        echo "Suma = $suma</br>";
        echo "Suma = $resta</br>";
        echo "Multiplicacion =$multiplicacion </br>";
        echo "Division =$division </br>";
        echo "<hr/>";
        
    if($negrita = true){
        echo '</h1>';
    }    
    
}

//llamada a la funcion

calculadora2(10, 2.5, true);
calculadora2(10, 75);

#################################################
#################################################



//FUNCIONES CON RETORNO(Opcion correcta no usar echo)

function devuelveNombre($name){
    
    return "el nombre es: $name";
}

//llamada a la funcion e imprimimos su contenido

echo devuelveNombre('Pepe Lotas');




function calculadora3($num1, $num2, $negrita= false) {
    
    $suma = $num1+$num2;
    $resta = $num1-$num2;
    $multiplicacion = $num1*$num2;
    $division = $num1/$num2;
    
    $cadena_texto = ''; //declaramos una cadena vacia para ir introduciendo el texto concatenando con .=
    
    if($negrita = true){
        $cadena_texto.= '<h1>'; //concatenamos en vez del echo
    }
        
        $cadena_texto.= "Suma = $suma</br>";
        $cadena_texto.= "Suma = $resta</br>";
        $cadena_texto.= "Multiplicacion =$multiplicacion </br>";
        $cadena_texto.= "Division =$division </br>";
        $cadena_texto.= "<hr/>";
        
    if($negrita = true){
        $cadena_texto.= '</h1>';
    }
    
    var_dump($cadena_texto);
    return $cadena_texto;
    
}

//llamada a la funcion e imprimimos su contenido
echo calculadora3(10, 20, true);


//FUNCIONES CON OTRA FUNCION DENTRO

function getNombre($nombre) {
    
    $texto = "El nombre es :$nombre";
    return $texto;         
}

function getApellidos($apellidos) {
    
    $texto = "Los apellidos son :$apellidos";
    return $texto;         
}


function obtenerNombre($nombre, $apellidos) {
    
    $texto = getNombre($nombre)
             ."</br>".
             getApellidos($apellidos);
           
    return $texto;          
    
}

//llamada a la funcion
echo obtenerNombre('Pepe', 'Lotas');