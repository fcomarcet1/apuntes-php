<?php

//debugg
$nombre = "Fco Marcet Prieto";
var_dump($nombre);


//fechas
echo date('d-m-Y');
echo time();


//matematicas
echo "raiz cuadrada de 10".sqrt(10);
echo "nº aleatorio entre 10 y 40".rand(10,40);
echo "nº PI".pi();
echo "redondeo", round(7.864454, 2);


//mas funciones generales

# gettype($var)
gettype($nombre);//obtiene tipo de dato

# is_numeric($numero)
if(is_numeric($numero)){
    echo "La variables es un numero";
}

# is_string($numero)
is_string($nombre);

# empty($numero)
if(empty($numero)){
    echo "La variables esta vacia";
}

# isset($numero)
if(isset($numero)){
    echo "La variables existe";
}else{
    echo "La variables No existe";
}

# unset elimina variables
$year = 2020;
unset($year);

# trim limpia espacios
$frase = "    mi   contenido    ";
var_dump(trim($frase));

# strlen cuenta caracteres de un string
$cadena="12345";
echo strlen($cadena);

#strpos encuentra posicion en un string
$cadena1="Esto es una frase";
echo strpos($cadena1, "frase");

#str_replace reemplaza contenido de un string
$string = str_repeat("frase", "oracion");
echo $string;

# convertir a May y minusculas
echo strlolower($string);
echo strloupper($string);
