<?php

##################################################################################################################
##################################################################################################################

function conexion_db( $config = array()) 
  {

    $conexion = mysqli_connect( $config['host'], $config['username'], $config['password'], $config['database']);
    mysqli_set_charset( $conexion, $config['encoding']);


    if (!$conexion) {
       
        echo "Error: No se pudo conectar a MySQL." . PHP_EOL ."<br/>";
        echo "errno de depuración: " . mysqli_connect_errno() . PHP_EOL."<br/>";
        echo "error de depuración: " . mysqli_connect_error() . PHP_EOL."<br/>";
        exit;
    }
    /*else { //comentar
        echo "Éxito: Se realizó una conexión apropiada a MySQL!." . PHP_EOL;
        echo "Información del host: " . mysqli_get_host_info($conexion) . PHP_EOL;
    } */

    return ( $conexion);

  }

  //llamada a la funcion
  // $connection = conexion_db( $config['database']);




##################################################################################################################
##################################################################################################################

  function close_db( $conexion) 
  {
    mysqli_close( $conexion);
    unset ( $conexion);
  }

##################################################################################################################
##################################################################################################################


  function Execute( $sql, $conexion) 
  {
    $return = mysqli_query( $conexion, $sql);
    return ( $return);
  }

##################################################################################################################
##################################################################################################################

  /*function ExecuteQuery( $sql, $conexion) 
  {      

    $result = mysqli_query( $conexion, $sql);
    if  ( $row = mysqli_fetch_array( $result, MYSQLI_BOTH)) 
    {
      do 
      {   
        $data[] = $row;
      }
      while ( $row = mysqli_fetch_array( $result, MYSQLI_BOTH));
    }
    else 
    {
      $data = null;
    }

    mysqli_free_result( $result);
    return ( $data);

  }
*/