<?php

/*
 //COOKIE
 Fichero de texto que se almacena en el cliente que navega en la web,
 con informacion del usuario con el fin de recordar o rastrear el 
 comportamiento del mismo
 */

//crear cookie
//setcookie("nombre", "valor que solo puede ser texto", caducidad, ruta, dominio)

//cookie basica
//$name_cookie="micookie";
//$value_cookie="valor de mi galleta";
//setcookie($name_cookie, $value_cookie );

setcookie("micookie", "valor de mi galleta" );

//cookie con expiracion de tiempo (1año)
//$expires_cookie=(time()+(60*60*24*365));
setcookie("unyear", "valor de mi cookie de 365 dias", time()+(60*60*24*365));

?>

<a href="ver_cookies.php">Ver cookies</a>