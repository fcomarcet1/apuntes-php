<!DOCTYPE html>
<!-Comentario -->
<html>
    <head>
        <title>Validacion formulario </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>Validacion de formularios</h1>
        
        <?php 
        
        if(isset($_GET['error'])){
            
            $error = $_GET['error'];
            
            
            if($error == "error"){
                
                echo '<strong style="color:red">Faltan datos por insertar, Inserta todos los campos</strong>';
            } 
            
            if($error == "nombre"){
                
                echo '<strong style="color:red">Introduce correctamente el nombre</strong>';
            } 
            if($error == "apellidos"){
                
                echo '<strong style="color:red">Introduce correctamente los apellidos</strong>';
            } 
            if($error == "edad"){
                
                echo '<strong style="color:red">Introduce correctamente la edad</strong>';
            } 
            if($error == "mail"){
                
                echo '<strong style="color:red">Introduce correctamente el email</strong>';
            }
            if($error == "password"){
                
                echo '<strong style="color:red">Introduce una contraseña de mas de 5 caracteres</strong>';
            } 
        }
        
        ?>
          <form action="procesar_form.php" method="POST" enctype="multipart/form-data">
            
            <label for="nombre">Nombre</label><br/>
            <input type="text" name="nombre"  placeholder="Escribe tu nombre" pattern="[A-Za-z ]+" autofocus="autofocus" required="required"/><br/>
            <br/>
            
            <label for="apellidos">Apellidos</label><br/>
            <input type="text" name="apellidos"  placeholder="Escribe tus apellidos" pattern="[A-Za-z]+" required="required" /><br/>
            <br/>
            
            <label for="edad">Edad</label><br/>
            <input type="number" name="edad" placeholder="selecciona tu edad" pattern="[0-9]+"  required="required"/><br/>
            <br/>
            
            <label for="email">Email</label><br/>
            <input type="email" name="email"  placeholder="Escribe tu Email" pattern=".+@*.*" required="required" /><br/>
            <br/>
            
            <label for="contraseña">Contraseña</label><br/>
            <input type="password" name="password"  placeholder="Escribe tu contraseña" minlength="5" required/><br/>
            <br/>
            
            <input type="submit" name="enviar" value="Enviar "/>
        </form>
        
        
    </body>
</html>
