<?php
require_once 'funciones/validaciones.php';

if (($_SERVER['REQUEST_METHOD'] === 'POST') && (isset($_POST['submit']))) {

    //var_dump($_POST['submit']);
    
    //inicializacion variables
    $nombreError = $emailError = $apellidosError = $edadError = $websiteError = "";
    $dniError = $dniError2 = $passwordError = $descriptionError = $genderError = "";

    $nombre = $email = $apellidos = $edad = $dni = $password = $website = $description = "";
    $gender = $remember = "";

    if (isset($_POST["nombre"]) && isset($_POST["apellidos"]) && isset($_POST["edad"]) &&
            isset($_POST["email"]) && isset($_POST["dni"]) && isset($_POST["password"]) &&
            isset($_POST["website"]) && isset($_POST["description"]) && isset($_POST["gender"])) {

        

        $nombre = validate($_POST["nombre"]);
        $apellidos = validate($_POST["apellidos"]);
        $edad = validate($_POST["edad"]);
        $email = validate($_POST["email"]);
        $dni = validate($_POST["dni"]);
        $password = validate($_POST["password"]);
        $remember = validate($_POST['remember']);
        $website = validate($_POST['website']);


        

        //name validation
        if (empty($_POST['nombre'])) {

            $nombreError = 'Completa en campo nombre';
        } else {
            //$nombre = validate($_POST['nombre']);
            if (!preg_match('/^[a-zA-Z\s]+$/', $nombre)) {

                $nombreError = 'El nombre solo puede contener letras y espacios en blanco';
            }
        }

        //last name validation
        $apellidos = validate($_POST['apellidos']);
        if (!preg_match('/^[a-zA-Z\s]+$/', $apellidos)) {

            $apellidosError = 'los apellidos solo pueden contener letras y espacios en blanco';
        }

        //Email Validation
        if (empty($_POST['email'])) {
            $emailError = 'Por favor introduzca su email';
        } else {
            //$email = validate($_POST['email']);
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

                $emailError = 'Email no valido';
            }
        }

        //edad
        // Validación de la edad: Comprueba si el dato ha sido introducido, si es un número y si es mayor de edad.
        if (empty($_POST['edad'])) {

            $edadError = 'Falta por rellenar el campo "Edad"<br>';
        } else {
            if (!is_numeric($_POST['edad'])) {

                $edadError = "Escriba correctamente su edad.<br>";
            }
            if ($_POST['edad'] < 18) {

                $edadError = "El registro es para mayores de 18 años.<br>";
            }
        }

        //Validación del DNI (El DNI español son 8 caracteres numéricos y una letra): Comprueba si el dato ha sido introducido y si es la letra correcta.    
        if (empty($_POST['dni'])) {

            $dniError = 'Falta por rellenar el campo "DNI"<br>';
        } else {

            //Se separan los números de la letra.Lo ponemos en mayúscula.
            $letraDNI = strtoupper(substr($_POST['dni'], 8, 9));
            $numDNI = substr($_POST['dni'], 0, 8);

            //Se calcula la letra correspondiente al número    
            $letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
            $letraCorrecta = $letras[$numDNI % 23];

            if ((strlen($_POST['dni']) < 9) || (strlen($_POST['dni']) > 9)) {

                $dniError = "Ha introducido un DNI incorrecto<br>";
            } else if ($letraDNI != $letraCorrecta) {

                $dniError = "Ha introducido una letra incorrecta en el DNI<br>";
            }
        }

        //URL
        //$website = !empty($_POST['website']) ? (string) filter_var($_POST['website'], FILTER_VALIDATE_URL) : "";
        
        // Validate url
        $url_result = check_url($website);
        
        if($url_result == 'No es una URL valida'){
            $websiteError = $url_result;
            
        }
        //password
        if (empty($_POST['password'])) {

            $passwordError = 'El campo passoword no puede estar vacio';
        } else {
            //$password = validate($_POST['password']);

            if (strlen($password) < 6) {

                $passwordError = 'Por favor su contraseña ha de tener almenos 6 caracteres';
            }
        }

        //ARRREGLAR PARA CUANDO NO SE PULSE MOSTRAR ERROR
        // check box
        //$remember = validate($_POST['remember']);
        $remember = filter_var($remember, FILTER_VALIDATE_BOOLEAN);
        // now $remember is a boolean
        
        
        //descripcion
        $description = !empty($_POST['description']) ? validate($_POST['description']) : "";


        //genero
        if (empty($_POST['gender'])) {

            $genderError = 'Por favor introduce tu genero';
        } else {
            $gender = $_POST['gender'];
        }

        //validamos que no exixten errores

        if (empty($nombreError) && empty($apellidosError) && empty($emailError) &&
                empty($edadError) && empty($dniError) && empty($passwordError) &&
                empty($descriptionError) && empty($genderError) && empty($passwordError)) {

            // great form filling
            echo "Formulario enviado correctamente!";
            echo "<br>
        Nombre: $nombre <br>
        Apellidos: $apellidos <br>
        Edad: $edad <br>
        Email: $email <br>
        Dni: $dni <br>
        Password: $password <br>
        Website: $website <br>
        Description: $description <br>
        Gender: $gender <br>
        Remember Me: $remember
        ";
            exit(); // terminamos script
        }
    } else {

        //error no llegaron los parametros
    }
}
?>

<html>
    <head>
        <title>Complete Form</title>
        <style type="text/css">
            .error {
                color:red;
            }
        </style>
    </head>
    <body>
        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
            Name: 
            <input type="text" name="nombre" required="required" pattern="[A-Za-z]+" autofocus="autofocus" value="<?php if (isset($nombre)) echo $nombre ?>"> 
            <span class="error"><?php if (isset($nombreError)) echo $nombreError ?></span><br>
            Apellidos: 
            <input type="text" name="apellidos"  required="required" pattern="[A-Za-z]+" value="<?php if (isset($apellidos)) echo $apellidos ?>"> 
            <span class="error"><?php if (isset($apellidosError)) echo $apellidosError ?></span><br>
            Edad: 
            <input type="text" name="edad" required="required" pattern="[0-9]+" value="<?php if (isset($edad)) echo $edad ?>"> 
            <span class="error"><?php if (isset($edadError)) echo $edadError ?></span><br>
            Email: 
            <input type="text" name="email" pattern=".+@*.*" required="required" value="<?php if (isset($email)) echo $email ?>"> 
            <span class="error"><?php if (isset($emailError)) echo $emailError ?></span><br>
            DNI: 
            <input type="dni" name="dni" pattern="[0-9]{8}[A-Za-z]{1}" value="<?php if (isset($dni)) echo $dni ?>"> 
            <span class="error"><?php if (isset($dniError)) echo $dniError ?></span><br>
            Password: 
            <input type="password" name="password" required="required"> 
            <span class="error"><?php if (isset($passwordError)) echo $passwordError ?></span><br>
            Website: 
            <input type="text" name="website" placeholder=" Ej:https://www.miweb.com" pattern="^(http(s)?:\/\/)+[\w\-\._~:\/?#[\]@!\$&'\(\)\*\+,;=.]+$"  required="required" value="<?php if (isset($website)) echo $website ?>"><br>
            Description: 
            <textarea name="description" ><?php if (isset($description)) echo $description ?></textarea> <br>
            Gender: 
            Male
            <input type="radio" name="gender" value="male" <?php if (isset($gender) && $gender === "male") echo "checked" ?>> 
            Female
            <input type="radio" name="gender" value="female" <?php if (isset($gender) && $gender === "female") echo "checked" ?>>

            <span class="error"><?php if (isset($genderError)) echo $genderError ?></span> <br>
            Remember Me: 
            <input type="checkbox" name="remember">
            <input type="submit" name="submit">
        </form>
    </body>
</html>