
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
    </head>
    <body>
        <!-- VALIDAR CAMPOS DE UN FORMULARIO CON HTML5 -->
        <form name="miFormulario"  method="POST" action="validar.php" >
            <label for="nombre">Nombre</label><br> 
                <input type="text" name="nombre" required><br>
            <label for="edad">Edad</label><br> 
                <input type="number" name="edad" min="18" required><br>
            <label for="cp">Código postal</label><br> 
                <input type="text" name="cp"  pattern="(^([0-9]{5,5})|^)$" title="Ejemplo: 28000"><br>
            <label for="dni">DNI</label><br> 
                <input type="text" name="dni" required="required"  pattern="((\d{8})([-]?)([A-Z]{1}))" title="Ejemplo: 123456789A"><br>
            <label for="email">Email</label><br> 
                <input type="email" name="email" required><br>
            <label for="so">Sistema Operativo</label><br>
            <select name="so" required>
                <option value="">Seleccione SO</option>
                <option value="Windows">Windows</option>
                <option value="MacOS">MacOS</option>
                <option value="Linux">Linux</option>
            </select><br> 
            <label for="horas">Horas programación</label><br>
                <input type="radio" name="horas" value="<1" >1h 
                <input type="radio" name="horas" value="1-4" >1-4h
                <input type="radio" name="horas" value="5-8" >5-8h
                <input type="radio" name="horas" value="+8" >+8h
            <br> 
            <input type="checkbox" name="condiciones" required><label for="condiciones">Acepto enviar este formulario</label><br> 
            <input type="submit" value="Enviar" name="enviar">
        </form>

    </body>
</html>