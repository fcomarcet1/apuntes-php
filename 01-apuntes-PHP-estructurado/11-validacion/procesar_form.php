<?php
if (isset($_POST['enviar'])) {

    $error = "";

    if (!empty($_POST["nombre"]) && !empty($_POST["apellidos"]) &&
            !empty($_POST["edad"]) && !empty($_POST["email"]) && !empty($_POST["password"])) {

        $error = "not_error";

        $nombre = $_POST["nombre"];
        $apellidos = $_POST["apellidos"];
        $edad = (int)$_POST["edad"];
        $email = $_POST["email"];
        $password = $_POST["password"];

        //Validar nombre
        if ((!is_string($nombre)) || (preg_match("/[0-9]+/", $nombre))) {
            $error = "nombre";
        }
        
        //validar apellidos
        if ((!is_string($apellidos)) || (preg_match("/[0-9]+/", $apellidos))) {
            $error = "apellidos";
        }
        
        //edad
        if ((!is_int($edad)) || (!filter_var($edad, FILTER_VALIDATE_INT))) {
            $error = "edad";
        }
        
        //email
        if ((!is_string($email)) || (!filter_var($email, FILTER_VALIDATE_EMAIL))) {
            $error = "email";
        }
        
        //passwd
        if((empty($password)) || (strlen($password)< 5)){
            $error = "password";
        }
        
       /* debug
        var_dump($_POST);
        var_dump($error);
        die();
        */
        
    } else {

        $error = "error";
    }
}

if ($error != "not_error") {

    header("Location:index.php?error=$error");
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <!-- si no existen errores --> 
        <?php if ($error == 'not_error'): ?>

            <h1>datos validados correctamente</h1>
            <p><?= $nombre ?></p>
            <p><?= $apellidos ?></p>
            <p><?= $edad ?></p>
            <p><?= $email ?></p>
        <?php endif; ?>    



    </body>
</html>