<?php

require_once 'funciones/funciones.php';

$resultado = "";

    if ((isset($_POST['numero1'])) && (isset($_POST['numero2']))) {

        $resultado = false; 
        $numero1 = $_POST['numero1'];
        $numero2 = $_POST['numero2'];

        
        if (isset($_POST['suma'])) {
            $resultado = suma($numero1, $numero2);
            
        }

        if (isset($_POST['resta'])) {
            $resultado= resta($numero1, $numero2);
            
        }
        if (isset($_POST['multiplicacion'])) {
            $resultado = multiplicar($numero1, $numero2);
            
        }
        if (isset($_POST['division'])) {
            $resultado = division($numero1, $numero2);
            
        }
    }


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculadora Php</title>
</head>
<body>
    <h1>Calculadora php</h1>

        <form action="" method="POST">
            <label for="numero1">Numero 1</label>
            <input type="number" name="numero1" placeholder="Introduce el 1º valor"><br/>

            <label for="numero2">Numero 2</label>
            <input type="number" name="numero2" placeholder="Introduce el 2º valor"><br/>
            <br/>
            <input type="submit" value="Sumar" name="suma"/>
            <input type="submit" value="Restar" name="resta"/>
            <br/>
            <input type="submit" value="Multiplicar" name="multiplicacion"/>
            <input type="submit" value="Dividir" name="division"/>
        </form>
    <?php
        if ($resultado != false) {
            echo "El reultado es: $resultado ";
        }
    ?>
        
        
</body>
</html>