<?php
/*
    1. crear una sesion que aumente su valor en 1 o disminuya en 1 en funcion del parametro $_GET['counter'], esta 
    a 0 o a 1
*/

session_start();

//si no existe la session la creamos
if (!isset($_SESSION['numero'])) {

    $_SESSION['numero'] = 0;
}


if (isset($_GET['counter']) ) {

    if ($_GET['counter'] == 1) {

        $_SESSION['numero']++;
        
    }
    elseif($_GET['counter'] == 0){

        $_SESSION['numero']--;

    }
   
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <h1>El valor de mi session es: <?php echo $_SESSION['numero']; ?></h1>

    <a href="ejercicio1.php?counter=1">Aumentar valor session.</a><br/>
    <a href="ejercicio1.php?counter=0">Disminuir valor session.</a><br/>



</body>
</html>