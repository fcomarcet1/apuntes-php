<?php 

/* 
    1. Una funcion
    2. validar un email con filter_var
    3.recoger un variable por get y validarla
    4. mostrar el resultado.

*/

function validarEmail($email) {

    $estatus =" email no valido";
    if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {

        $estatus = " email valido";
    }

return $estatus;    
}


if (isset($_GET['email'])) {

    echo validarEmail($_GET['email']);
    
}
else {
    echo "introduce un parametro por get";
}



?>