<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    if (isset($_POST['submit'])) {

        //var_dump($_POST);
        //$_FILES es un array multidimensional (array que contiene otros arrays)
        $path_dir_upload = "imagenes/";
        $path_file_upload = $path_dir_upload . basename($_FILES["subida_fichero"]["name"]);
        $uploadOk = 1;
        $refresh = "Refresh:3;URL=index.php";

        //obtiene tipo imagen
        //$imageFileType = strtolower(pathinfo($path_file_upload, PATHINFO_EXTENSION));

        $archivo = $_FILES['subida_fichero'];
        $nombre = $archivo['name']; //para acceder al nombre usamos $nombre = $_FILES['subida_fichero']['name'];
        $tipo = $archivo['type'];


        //debugg
        // print_r($_FILES);
        //var_dump($archivo);
        //echo "$archivo";
        //die();
        
        
        
        //subida de solo imagenes
        if (($tipo == "image/jpg") || ($tipo == "image/jpeg") || ($tipo == "image/png") ||
            ($tipo == "image/git")) {

                //$uploadOk = 1;
                //echo "La imagen se subio correctamente";
                
                //comprobamos si no existe el directorio y lo creamos.
                if(!is_dir($path_dir_upload)){
                    mkdir($path_dir_upload, 0777);   
                }
                
                //mediante move_uploaded_file pasamos el fichero del dir tmp al indicado.
                
                move_uploaded_file($archivo['tmp_name'], $path_file_upload);
                echo "<h1> imagen subida correctamente </h1>";
                header($refresh);
        } 
        else {

                $uploadOk = 0;
                header($refresh);
                echo "<h1>Sube una imagen con un formato correcto</h1>";
        }
    }

 

   
    
}

