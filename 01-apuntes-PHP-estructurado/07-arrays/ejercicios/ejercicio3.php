<?php

/* 
 Crear un array con el contenido de la tabla:
  ACCION   AVENTURA            DEPORTES
  GTA      ASSASINS            FIFA
  COD      CRASH               PES
  PUBG     PRINCE OF PERSIA    NBA2K20
  
 Cada fila debe ir en un fichero separado(include)
 */


$tabla = array(
    
    'ACCION'=>array('GTA','COD','PUBG'),
    'AVENTURA'=>array('ASSASINS','CRASH','PRINCE OF PERSIA '),
    'DEPORTES'=>array('FIFA','PES','NBA2K20')
);

//var_dump($tabla);
//var_dump(array_keys($tabla);

/*
<?php foreach ($categorias as $categoria) { ?>
            <th></th>
        <?php } ?>
 
 * SON SIMILARES MEJOR USAR 2º
 
 <?php foreach ($categorias as $categoria) : ?>
            <th></th>
  <?php endforeach; ?>
  
 */


$categorias = array_keys($tabla);
?>

<table border="1">
    <?php require_once 'ejercicio3/encabezado.php'; ?>
    <?php require_once 'ejercicio3/primera.php'; ?>
    <?php require_once 'ejercicio3/segunda.php'; ?>
    <?php require_once 'ejercicio3/tercera.php'; ?>
    
</table>
