<?php

/* 
 ARRAYS.
 */
$peliculas=array('Batman','Spiderman','Thor','Esla');
//var_dump($peliculas);

echo "$peliculas[0]</br>";
echo "$peliculas[1]</br>";
echo "$peliculas[2]</br>";
echo "$peliculas[3]</br>";

echo "<hr/>";

$cantantes = ['El Fary', 'Snoop Dog', 'Beyonce'];
//var_dump($cantantes);
echo "$cantantes[2]</br>";

echo "<hr/>";

//Recorrer arrays -> for
echo "<h1>Listado de peliculas</h1>";
echo "<ul>";

for ($i=0; $i<count($peliculas); $i++){
    
    echo "<li>".$peliculas[$i]."</li>";
}
echo "</ul>";

echo "<hr/>";

//Recorrer arrays -> foreach
echo "<h1>Listado de cantantes</h1>";

echo "<ul>";

foreach ($cantantes as $cantante) {
    echo "<li>".$cantante."</li>";
}
echo "</ul>";
echo "<hr/>";

//ARRAYS ASOCIATIVOS (los indices no son un numero)
$personas=array(
    
    'nombre'=>'Francisco',
    'apellidos'=>'Marcet prieto',
    'web'=>'www.fcomarcet.es'
    
);
var_dump($personas);

// echo $personas[1]; --> ERROR
echo $personas['apellidos'];

echo "<hr/>";

//ARRAYS MULTIDIMENSIONALES
$contactos =array(
    array(
        'nombre'=>'Antonio',
        'email'=>'antonio@mail,com'   
    ),
    array(
        'nombre'=>'Paco',
        'email'=>'paco@mail,com'   
    ),
    array(
        'nombre'=>'pepe',
        'email'=>'pepe@mail,com'   
    ),
    
    
);

var_dump($contactos);

//obtener a paco
echo $contactos[1]['nombre']."</br>";

//email de pepe
echo $contactos[2]['email']."</br>";


echo "<hr/>";

//RECORRER ARRAY MULTIDIMENSIONAL

foreach ($contactos as $key => $contacto) {
    
    echo $contacto['nombre']."</br>";
    
}